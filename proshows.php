<!DOCTYPE html>
<html>
<head>
	<title>Proshows | Incident - NITK Surathkal </title>
	<meta name="description" content="Incident Proshows brings you top artists from across the globe. From India's no.1 band, The Local Train to India's best DJs DJ Chetas and Lost Stories. 
	So come witness the best artists performing in Incident at NITK Surathkal"/>
	<?php include_once("headers.php") ?>
	<link rel="stylesheet" type="text/css" href="css/events.min.css">
	<link rel="stylesheet" type="text/css" href="css/02-proshows.min.css">
	<link rel="stylesheet" type="text/css" href="css/events.media.min.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>  
	<script src="js/events.min.js"></script>
	<script async src="js/proshows.min.js"></script>
</head>
<body>
<?php include_once("loader.php"); ?>
<?php include_once("menu.php"); ?>
<main>
<div class="side-bar">
	<h1>Proshows</h1>
	<ul>
		<li class="active" data-target="#karthick-iyer">
			<span class="underline animate" >Karthick Iyer</span>
		</li>
		<li data-target="#baiju-dharmajan">
			<span class="underline" >Baiju Dharmajan</span>
		</li>
		<li data-target="#masala-coffee">
			<span class="underline" >Masala Coffee</span>
		</li>
		<li data-target="#papa-cj">
			<span class="underline" >Papa CJ</span>
		</li>
		<li data-target="#redbull-tour-bus">
			<span class="underline" >RedBull Tour Bus</span>
		</li>
		<li data-target="#local-train">
			<span class="underline" >The Local Train</span>
		</li>
		<li data-target="#lost-stories">
			<span class="underline" >Lost Stories</span>
		</li>
		<li data-target="#dj-chetas">
			<span class="underline" >DJ Chetas</span>
		</li>
	</ul>
	<div class="scroll-bar"></div>
</div>
<div class="events">
	<div class="event" id="karthick-iyer">
		<img src="images/events/karthick-iyer.jpg" class="event-bg">
		<h1>Karthick Iyer</h1>
		<p class="event-description">Forging a new sound with his soulful and emotion driven violin performances, 
		Karthick Iyer is a freestyle musician known for taking Indian music to the world. With over 20 years of
		 experience in the field of classical carnatic music, he has been delivering enthralling performances 
		 with his style of music.</p>
		 <div class="more-details">
		 	<div class="playlist">
			 	<h2>Playlist</h2>
			 	<p class="active song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=uLsvlZLDisY">Nagumomo</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=wcuYU8D1GhY">Bollwood Medley</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=n04krec26PY">Rejoicing in Raghuvamsa</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=Y8jQjOnhWzI">An Ilayaraja</a></p>
			 	<p class="song more-link"><a target="_blank" rel="nofollow" href="https://www.youtube.com/user/Thekarthickiyer">More</a></p>
		 	</div>
		 	<div class="right-half">
		 		<div class="date">Date: 01 March 2017</div>
		 		<div class="book-ticket"><a target="_blank" rel="nofollow" href="https://in.bookmyshow.com/events/classical-night-by-karthick-iyer-live/ET00053662">BOOK NOW</a></div>
		 		<iframe class="frame-fit" src="https://www.youtube.com/embed/uLsvlZLDisY"></iframe>
		 	</div>
		 </div>
	</div>
	<div class="event" id="baiju-dharmajan">
		<img src="images/events/baiju-dharmajan.jpg" class="event-bg">
		<h1>Baiju Dharmajan</h1>
		<p class="event-description">One of India’s most revered and celebrated musicians, Baiju Dharmajan
		 is widely recognized for his unique ability of seamlessly blending overtones of Carnatic music with rock. 
		 Baiju infused his carnatic inspired guitar playing styles into compositions with Indian carnatic classical
		  music concepts and melodies that he creates with the Baiju Dharmajan Syndicate.</p>
		 <div class="more-details">
		 	<div class="playlist">
			 	<h2>Playlist</h2>
			 	<p class="active song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=9o0c0aItpKo">Sweet Indian Child of Mine</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=uYurzVLqC_Y">Vaishnava Janato</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=pk2Bwhl7LFg">Kaithola</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=u-ZTETprEjI">Mile Sur Mera Tumhara</a></p>
			 	<p class="song more-link"><a target="_blank" rel="nofollow" href="https://www.youtube.com/user/Baijuofficial">More</a></p>
		 	</div>
		 	<div class="right-half">
		 		<div class="date">Date: 02 March 2017</div>
		 		<div class="book-ticket"><a target="_blank" rel="nofollow" href="https://in.bookmyshow.com/events/rock-night-by-baiju-dharmajan-followed-by-eastern-night-by-masala-coffee/ET00053663">BOOK NOW</a></div>
		 		<iframe class="frame-fit" src="https://www.youtube.com/embed/9o0c0aItpKo"></iframe>
		 	</div>
		 </div>
	</div>
	<div class="event" id="masala-coffee">
		<img src="images/events/masala-coffee.jpg" class="event-bg">
		<h1>Masala Coffee</h1>
		<p class="event-description">
			"Kaanthaa njanum varaam... thrissuur pooram kaanaan..."<br/>
			Reminds you of a masterpiece? Get set to witness them live at Incident'17!<br/>
			Masala Coffee is an eastern band that performs in the genres of Indian folk, pop and rock. 
			They rose to popularity through music videos on the popular music show Music Mojo at KappaTV and 
			their YouTube videos have been garnering millions of views. They have covered popular songs in 
			Malayalam, Hindi and Tamil. Creating new music with instruments and blending in the folk culture 
			in every composition, the band has been creating magic with every performance. 
		</p>
		 <div class="more-details">
		 	<div class="playlist">
			 	<h2>Playlist</h2>
			 	<p class="active song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=B3hlqsBY0Qk">Kaanthaa</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=JZzgOPaLuGw">Aalayal Thara Venam</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=X9RKX4IzNVw">Munbe Vaa</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=AB2ACJ22fhY">Snehithane</a></p>
			 	<p class="song more-link"><a target="_blank" rel="nofollow" href="https://www.youtube.com/channel/UCjfsSezSDOOzF4B5IFHD2WQ/videos">More</a></p>
		 	</div>
		 	<div class="right-half">
		 		<div class="date">Date: 02 March 2017</div>
		 		<div class="book-ticket"><a target="_blank" rel="nofollow" href="https://in.bookmyshow.com/events/rock-night-by-baiju-dharmajan-followed-by-eastern-night-by-masala-coffee/ET00053663">BOOK NOW</a></div>
		 		<iframe class="frame-fit" src="https://www.youtube.com/embed/B3hlqsBY0Qk"></iframe>
		 	</div>
		 </div>
	</div>
	<div class="event" id="papa-cj">
		<img src="images/events/papa-cj.jpg" class="event-bg">
		<h1>Papa CJ</h1>
		<p class="event-description">
			Engineering, they say, is definitely a roller coaster ride - sailing through thick and thin and 
			battling all odds is a Herculean task ! Throughout this voyage, one must never forget to contract 
			the chin muscles and upward curve your lips. Smile, Laugh and make the world Laugh.<br/>
			Tickle your bones, let your jaws drop and eyes water. Incident 2017 presents to you Papa CJ - Stand 
			Up Comedian. Get your gang and head right here. The perfect stress buster!
		</p>
		 <div class="more-details">
		 	<div class="playlist">
			 	<h2>Videos</h2>
			 	<p class="active song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=KgnthWjJLZY">A Humorous Inspiration</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=tGv0plOw2uM">Comedy As an Instrument of Protest</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=ZziavFHt8kE">Papa CJ in London</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=_np62NuM1Zo">Papa CJ at St.Xaviers</a></p>
			 	<p class="song more-link"><a target="_blank" rel="nofollow" href="https://www.youtube.com/user/papacj/videos">More</a></p>
		 	</div>
		 	<div class="right-half">
		 		<h2>Date: 03 March 2017</h2>
		 		<iframe class="frame-fit" src="https://www.youtube.com/embed/KgnthWjJLZY"></iframe>
		 	</div>
		 </div>
	</div>
	<div class="event" id="redbull-tour-bus">
		<img src="images/events/redbull-tour-bus.jpg" class="event-bg">
		<h1>RedBull Tour Bus</h1>
		<p class="event-description">
			Surprises are unending this Incident. And the biggest might just arrive as you're taking a casual stroll. 
			The band you hear playing in the background, might actually be there playing live, on the Sassy Red Bull Tour 
			Bus. Yes, be ready to witness magic and live music at unexpected places this Incident, cause The Red Bull Tour 
			Bus, is visiting NITK Surathkal, and on top of it will be <u><b>Lagori</b></u> and <u><b>DJ Oceantied</b></u>. So if you hear the music grow 
			louder, you better turn around.
		</p>
		 <div class="more-details">
		 	<div class="playlist">
			 	<h2>Playlist</h2>
			 	<p class="active song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=_QWbqpMsA0o">Lagori - Pardesi Cover</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=icRTHGBnqwE">Lagori - Boom Shankar</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=lAPxzMgDns8">Lagori - Mast Kalandar</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=HsFwdHbiCWc">Oceantied - Boiler Room</a></p>
		 	</div>
		 	<div class="right-half">
		 		<div class="date">Date: 03 March 2017</div>
		 		<div class="book-ticket">
		 			Venue: NITK Main Ground
		 		</div>
		 		<iframe class="frame-fit" src="https://www.youtube.com/embed/_QWbqpMsA0o"></iframe>
		 	</div>
		 </div>
	</div>
	<div class="event" id="local-train">
		<img src="images/events/the-local-train.jpg" class="event-bg">
		<h1>The Local Train</h1>
		<p class="event-description">Bringing a fresh twist to 'Hindie' rock music, The Local Train has been winning awards and hearts since 2008. 
		They have been ranked as India's Number 1 Rock Band by Sennheiser, and have released a series of hits like Choo Lo, Aaoge Tum Kabhi, 
		Yeh Zindagi Hai, and Kaisey Jiyun. </p>
		 <div class="more-details">
		 	<div class="playlist">
			 	<h2>Playlist</h2>
			 	<p class="active song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=uB5bf7LQPVU">Choo Lo</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=qLCLvzTGFVM">Dil Mere</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=i96UO8-GFvw">Aaoge Tum Kabhi</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=8k0FFrtKnpQ">Kaisey Jiyun</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=SEqQCIMQfk0">Yeh Zindagi Hai</a></p>
			 	<p class="song more-link"><a target="_blank" rel="nofollow" href="https://www.youtube.com/user/TLTOfficial">More</a></p>
		 	</div>
		 	<div class="right-half">
		 		<div class="date">Date: 04 March 2017</div>
		 		<div class="book-ticket"><a target="_blank" rel="nofollow" href="https://in.bookmyshow.com/events/popular-night-by-the-local-train/ET00053801">BOOK NOW</a></div>
		 		<iframe class="frame-fit" src="https://www.youtube.com/embed/uB5bf7LQPVU"></iframe>
		 	</div>
		 </div>
	</div>
	<div class="event" id="lost-stories">
		<img src="images/events/lost-stories.jpg" class="event-bg">
		<h1>Lost Stories <label>powered by <a target="_blank" rel="nofollow" href="https://www.nestaway.com/"><img alt="Logo of Nestaway - Powering DJ Chetas" title="Logo of Nestaway - Powering DJ Chetas" src="images/sponsors/Nestaway.png"/></a></label></h1>
		<p class="event-description">
			'Not all those who wander are lost.' Loosen your feet, groove to the beats and jump hard as the Lost Stories drop the bass!<br/>
			The Indian DJ Duo, ranked #1 EDM artist in India and #52 among the top 100 DJs in the world by DJ Mag in 2016. They have performed in Tomorrowland, Mysteryland, Sunburn, Supersonic and many other music festivals worldwide.
			<br/>Known for their remixes of Alan Walker's "Faded", POTF's "Carnival of Rust", Justin Bieber's "Sorry" on their YouTube channel, the duo have emerged as trendsetters in Progressive House Music.
		</p>
		 <div class="more-details">
		 	<div class="playlist">
			 	<h2>Playlist</h2>
			 	<p class="active song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=sv81BgWl2U0">Faded</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=0A745C9Dmq4">Carnival of Rust</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=bZNrjIj0Ekw">Sorry</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=l9F_gwl3L3Y">Wherever I Go</a></p>
			 	<p class="song more-link"><a target="_blank" rel="nofollow" href="https://www.youtube.com/user/LostStoriesVEVO/videos">More</a></p>
		 	</div>
		 	<div class="right-half">
		 		<div class="date">Date: 05 March 2017</div>
		 		<div class="book-ticket"><a rel="nofollow" target="_blank" href="https://in.bookmyshow.com/events/edm-night-by-dj-lost-stories-bdm-night-by-dj-chetas/ET00053664">BOOK NOW</a></div>
		 		<iframe class="frame-fit" src="https://www.youtube.com/embed/sv81BgWl2U0"></iframe>
		 	</div>
		 </div>
	</div>
	<div class="event" id="dj-chetas">
		<img src="images/events/chetas.jpg" class="event-bg">
		<h1>DJ Chetas <label>powered by <a target="_blank" rel="nofollow" href="https://www.nestaway.com/"><img alt="Logo of Nestaway - Powering DJ Chetas" title="Logo of Nestaway - Powering DJ Chetas" src="images/sponsors/Nestaway.png"/></a></label></h1>
		<p class="event-description">
			Explore a whole new world of Bollywood Dance Music with The king of Mashups!<br/>
			The man whose music rules parties, a trendsetter showcasing Bollywood music like no other, DJ CHETAS is 
			India's most followed premier DJ and is currently ranked #33 among the DJMag Top 100 DJs in the world. 
			His performances are packed across the globe, promising an audio-visual extravaganza, with people grooving to 
			every beat and living the night of their lives.<br/>
			Come, be part of a night of absolute madness!</p>
		 <div class="more-details">
		 	<div class="playlist">
			 	<h2>Playlist</h2>
			 	<p class="active song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=58MWfGOWRhQ">Ikk Kudi vs Golden</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=EVAqgdGPapM">Nachde Ne Saare</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=-1d0XfIwV6U">Kaala Chashma</a></p>
			 	<p class="song"><i class="material-icons">play_circle_filled</i> <a target="_blank" rel="nofollow" href="https://www.youtube.com/watch?v=7jW1rggzZDA">Soch Na Saake vs Electric Glow</a></p>
			 	<p class="song more-link"><a target="_blank" rel="nofollow" href="https://www.youtube.com/user/DJCHETASOFFICIAL/videos">More</a></p>
		 	</div>
		 	<div class="right-half">
		 		<div class="date">Date: 05 March 2017</div>
		 		<div class="book-ticket"><a rel="nofollow" target="_blank" href="https://in.bookmyshow.com/events/edm-night-by-dj-lost-stories-bdm-night-by-dj-chetas/ET00053664">BOOK NOW</a></div>
		 		<iframe class="frame-fit" src="https://www.youtube.com/embed/58MWfGOWRhQ"></iframe>
		 	</div>
		 </div>
	</div>
</div>
</main>
</body>
</html>