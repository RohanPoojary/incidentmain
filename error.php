<!DOCTYPE html>
<html>
<head>
	<title>404 | Not Found</title>
	<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/error.css">
	<style>
	body {
		  margin: 0;
		  padding: 0;
		  position: relative;
		  background: #f3f1fa;
		  overflow: hidden;
		  text-align: center; }

		.bg {
		  position: absolute;
		  top: 0;
		  width: 100vw;
		  height: 100vh;
		  z-index: -5;
		  background: url(images/Logo.svg) 0 0 no-repeat;
		  background-size: contain;
		  background-position: center center;
		  opacity: 0.25; }

		main {
		  box-sizing: border-box;
		  position: relative; }
		  main h1 {
		    margin: 0;
		    padding-top: 7%;
		    font-family: "Lobster", sans-serif;
		    font-size: 8em;
		    color: #443188; }
		  main h2 {
		    margin: 0;
		    font-family: "Lobster", sans-serif;
		    font-size: 4em;
		    color: #573eae; }
		  main a {
		    display: block;
		    margin-top: 5%;
		    font-family: serif;
		    font-size: 1.5em;
		    font-weight: bold; }
	</style>
</head>
<body>
<div class="bg"></div>
<main>
	
	<h1>404</h1>
	<h2>Not Found</h2>
	<a href="./">Go back</a>
</main>
</body>
</html>