<!DOCTYPE html>
<html>
<head>
	<title>Sponsors | Incident 2017 - NITK Surathkal</title>
	<meta name="description" content="A list of Sponsors who are the backbone of Incident 2017."/>
	<?php include_once("headers.php") ?>
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.4/TweenMax.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/sponsors.min.css">
    <script src="js/sponsors.min.js"></script>
</head>
<body>
<?php //include_once("loader.php"); ?>
<?php include_once("menu.php"); ?>
<div class="background">
	<img class="responsive-img logo" alt="Logo of Incident" title="Logo of Incident" src="images/logo.svg">
</div>
<div class="container main">
	<h1 class="title">SPONSORS</h1>
	<br/>
	<h2 class="center">Co Sponsors</h2>
	<div class="row">
		<div class="col m4 s12 offset-m4">
			<a target="_blank" rel="nofollow" href="http://www.ril.com/">
				<img alt="logo of Co Sponsors of Incident - Reliance" title="logo of Co Sponsors of Incident - Reliance" src="images/sponsors/Reliance.png"/>
			</a>
		</div>
	</div>
	<h2 class="center">Iron Sponsors</h2>
	<div class="row">
		<div class="col m4 s12 offset-m4">
			<a target="_blank" rel="nofollow" href="http://www.mmlkar.com/">
				<img alt="logo of iron Sponsors of Incident - MML" title="logo of Iron Sponsors of Incident - MML" src="images/sponsors/MML.png"/>
			</a>
		</div>
	</div>
	<h2 class="center">Home Rental Partners</h2>
	<div class="row">
		<div class="col m4 s12 sponsors offset-m4">
			<a target="_blank" rel="nofollow" href="https://www.nestaway.com/">
				<img alt="logo of Home Rental Partners of Incident - Nestaway" title="logo of Home Rental Partners of Incident - Nestaway" src="images/sponsors/Nestaway.png"/>
			</a>
		</div>
	</div>
	<h2 class="center">Eternal Partners</h2>
	<div class="row">
		<div class="col m4 s12 sponsors offset-m4">
			<a target="_blank" rel="nofollow" href="https://nitkalumni.in/">
				<img alt="logo of Eternal Partners of Incident - NITK Alumni Association" title="logo of Eternal Partners of Incident - NITK Alumni Association" src="images/sponsors/NITK_Alumni_Association.png"/>
			</a>
		</div>
	</div>
	<h2 class="center">Ice Cream Partners</h2>
	<div class="row">
		<div class="col m4 s12 sponsors offset-m4">
			<a target="_blank" rel="nofollow" href="http://www.idealicecream.in/">
				<img alt="logo of Ice Cream Partners of Incident - Ideal" title="logo of Ice Cream Partners of Incident - Ideal" src="images/sponsors/Ideal_Ice_Cream.png"/>
			</a>
		</div>
	</div>
	<h2 class="center">Other Sponsors</h2>
	<div class="row">
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.ongcindia.com/wps/wcm/connect/ongcindia/home">
				<img alt="logo of Sponsor of Incident - ONGC" title="logo of Sponsor of Incident - ONGC" src="images/sponsors/ONGC.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://fintellix.com/">
				<img alt="logo of Sponsor of Incident - Fintellix" title="logo of Sponsor of Incident - Fintellix" src="images/sponsors/Fintellix.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://knkindia.com/">
				<img alt="logo of Sponsor of Incident - knk" title="logo of Sponsor of Incident - knk" src="images/sponsors/knk.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.solidworks.com/">
				<img alt="logo of Sponsor of Incident - Solid Works" title="logo of Sponsor of Incident - Solid Works" src="images/sponsors/Solid_Works.png"/>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.mtvindia.com/cokestudio/">
				<img alt="logo of Sponsor of Incident - CokeStudio" title="logo of Sponsor of Incident - CokeStudio" src="images/sponsors/CokeStudio.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.adani.com/">
				<img alt="logo of Sponsor of Incident - Adani" title="logo of Sponsor of Incident - Adani" src="images/sponsors/Adani.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.hindustanpetroleum.com/">
				<img alt="logo of Sponsor of Incident - HP" title="logo of Sponsor of Incident - HP" src="images/sponsors/HP.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.kcl.ac.uk/index.aspx">
				<img alt="logo of Sponsor of Incident - Kings College London" title="logo of Sponsor of Incident - Kings College London" src="images/sponsors/Kings_College_London.png"/>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.jsw.in/">
				<img alt="logo of Sponsor of Incident - JSW" title="logo of Sponsor of Incident - JSW" src="images/sponsors/JSW.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors ">
			<a target="_blank" rel="nofollow" href="https://www.nestle.in/">
				<img alt="logo of Sponsor of Incident - Nestle" title="logo of Sponsor of Incident - Nestle" src="images/sponsors/Nestle.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.kmfnandini.coop">
				<img alt="logo of Sponsor of Incident - Nandini" title="logo of Sponsor of Incident - Nandini" src="images/sponsors/Nandini.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://mangaloresez.com/">
				<img alt="logo of Sponsor of Incident - MSEZ" title="logo of Sponsor of Incident - MSEZ" src="images/sponsors/MSEZ.png"/>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="https://www.vodafone.in/">
				<img alt="logo of Sponsor of Incident - Vodafone" title="logo of Sponsor of Incident - Vodafone" src="images/sponsors/Vodafone.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://expertindus.com/">
				<img alt="logo of Sponsor of Incident - Expertise" title="logo of Sponsor of Incident - Expertise" src="images/sponsors/Expertise.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="https://www.sbi.co.in/">
				<img alt="logo of Sponsor of Incident - SBI" title="logo of Sponsor of Incident - SBI" src="images/sponsors/SBI.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.canarabank.com/">
				<img alt="logo of Sponsor of Incident - Canara Bank" title="logo of Sponsor of Incident - Canara Bank" src="images/sponsors/Canara_Bank.png"/>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.bmw.com/">
				<img alt="logo of Sponsor of Incident - BMW" title="logo of Sponsor of Incident - BMW" src="images/sponsors/BMW.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.ktm.com/">
				<img alt="logo of Sponsor of Incident - KTM" title="logo of Sponsor of Incident - KTM" src="images/sponsors/KTM.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.audi.com/index.html">
				<img alt="logo of Sponsor of Incident - Audi" title="logo of Sponsor of Incident - Audi" src="images/sponsors/Audi.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="https://www.zoomcar.com/">
				<img alt="logo of Sponsor of Incident - Zoomcar" title="logo of Sponsor of Incident - Zoomcar" src="images/sponsors/Zoomcar.png"/>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.youtube.com/channel/UC7IMq6lLHbptAnSucW1pClA">
				<img alt="logo of Sponsor of Incident - Filter Copy" title="logo of Sponsor of Incident - Filter Copy" src="images/sponsors/Filter_Copy.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.vijayabank.com/">
				<img alt="logo of Sponsor of Incident - Vijaya Bank" title="logo of Sponsor of Incident - Vijaya Bank" src="images/sponsors/Vijaya_Bank.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.karnatakabank.com/">
				<img alt="logo of Sponsor of Incident - Karnataka Bank" title="logo of Sponsor of Incident - Karnataka Bank" src="images/sponsors/Karnataka_Bank.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.mheducation.com/">
				<img alt="logo of Sponsor of Incident - MGH" title="logo of Sponsor of Incident - MGH" src="images/sponsors/MGH.png"/>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.resonancestudios.in/">
				<img alt="logo of Sponsor of Incident - Resonance Studios" title="logo of Sponsor of Incident - Resonance Studios" src="images/sponsors/Resonance_Studios.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="https://www.sprite.com/">
				<img alt="logo of Sponsor of Incident - Sprite" title="logo of Sponsor of Incident - Sprite" src="images/sponsors/Sprite.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="https://www.facebook.com/pages/Binary-Systems-Pvt-LtdMangalore/200738120095999">
				<img alt="logo of Sponsor of Incident - Binary" title="logo of Sponsor of Incident - Binary" src="images/sponsors/Binary.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.indywoodtalenthunt.com/">
				<img alt="logo of Sponsor of Incident - Indywood" title="logo of Sponsor of Incident - Indywood" src="images/sponsors/Indywood.png"/>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col m3 s6 sponsors ">
			<a target="_blank" rel="nofollow" href="http://www.jockeyindia.com/">
				<img alt="logo of Sponsor of Incident - Jockey" title="logo of Sponsor of Incident - Jockey" src="images/sponsors/Jockey.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="">
				<img alt="logo of Sponsor of Incident - SBC" title="logo of Sponsor of Incident - SBC" src="images/sponsors/SBC.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="https://vistamind.com">
				<img alt="logo of Sponsor of Incident - Vista Mind" title="logo of Sponsor of Incident - Vista Mind" src="images/sponsors/Vista_Mind.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="https://www.facebook.com/innerspacestudiosbangalore">
				<img alt="logo of Sponsor of Incident - Inner Space Studio" title="logo of Sponsor of Incident - Inner Space Studio" src="images/sponsors/Inner_Space_Studio.png"/>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.smokenoven.com/">
				<img alt="logo of Sponsor of Incident - Smoke N Oven" title="logo of Sponsor of Incident - Smoke N Oven" src="images/sponsors/Smoke_N_Oven.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://onelink.to/cloop">
				<img alt="logo of Sponsor of Incident - Cloop" title="logo of Sponsor of Incident - Cloop" src="images/sponsors/Cloop.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://bplusunisexsaloon.com/">
				<img alt="logo of Sponsor of Incident - BPlus" title="logo of Sponsor of Incident - BPlus" src="images/sponsors/BPlus.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="https://in.bookmyshow.com/">
				<img alt="logo of Sponsor of Incident - BookMyShow" title="logo of Sponsor of Incident - BookMyShow" src="images/sponsors/Book_My_Show.png"/>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.thebrandfactory.com/">
				<img alt="logo of Sponsor of Incident - Brand Factory" title="logo of Sponsor of Incident - Brand Factory" src="images/sponsors/Brand_Factory.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.sarvajnaonline.in/">
				<img alt="logo of Sponsor of Incident - SIA" title="logo of Sponsor of Incident - SIA" src="images/sponsors/SIA.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.mangaloremerijaan.com/">
				<img alt="logo of Sponsor of Incident - MMJ" title="logo of Sponsor of Incident - MMJ" src="images/sponsors/MMJ.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="istanbulcymbals.com">
				<img alt="logo of Sponsor of Incident - Istanbul Agop" title="logo of Sponsor of Incident - Istanbul Agop" src="images/sponsors/Istanbul_Agop.png"/>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col m3 s6 sponsors ">
			<a target="_blank" rel="nofollow" href="http://www.barbeque-nation.com/">
				<img alt="logo of Sponsor of Incident - Barbeque Nation" title="logo of Sponsor of Incident - Barbeque Nation" src="images/sponsors/Barbeque_Nation.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="https://www.facebook.com/TheForumFizaMall">
				<img alt="logo of Sponsor of Incident - Form Fiza Mall" title="logo of Sponsor of Incident - Form Fiza Mall" src="images/sponsors/Form_Fiza_Mall.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://creambell.co.ke/">
				<img alt="logo of Sponsor of Incident - Cream Bell" title="logo of Sponsor of Incident - Cream Bell" src="images/sponsors/Cream_Bell.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.mcdonaldsindia.com/">
				<img alt="logo of Sponsor of Incident - McDonalds" title="logo of Sponsor of Incident - McDonalds" src="images/sponsors/McDonalds.png"/>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="https://pepperosalt.com/">
				<img alt="logo of Sponsor of Incident - Pepper O Salt" title="logo of Sponsor of Incident - Pepper O Salt" src="images/sponsors/Pepper_O_Salt.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.cinepolisindia.com/">
				<img alt="logo of Sponsor of Incident - Cinepolis" title="logo of Sponsor of Incident - Cinepolis" src="images/sponsors/Cinepolis.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="http://www.maroor.in/">
				<img alt="logo of Sponsor of Incident - Maroors" title="logo of Sponsor of Incident - Maroors" src="images/sponsors/Maroors.png"/>
			</a>
		</div>
		<div class="col m3 s6 sponsors">
			<a target="_blank" rel="nofollow" href="https://www.inshorts.com/en/read/">
				<img alt="logo of Sponsor of Incident - Inshorts" title="logo of Sponsor of Incident - Inshorts" src="images/sponsors/Inshorts.png"/>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col m3 s6 sponsors offset-m4">
			<a target="_blank" rel="nofollow" href="http://www.corpbank.com/">
				<img alt="logo of Sponsor of Incident - Corporation Bank" title="logo of Sponsor of Incident - Corporation Bank" src="images/sponsors/Corporation_Bank.png"/>
			</a>
		</div>
		
	</div>
</div>
</body>
</html>
