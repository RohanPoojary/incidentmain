<!DOCTYPE html>
<html>
<head>
	<title>Events | Incident - NITK Surathkal</title>
	<meta name="description" content="Incident 2017 hosts for a variety of events and competitions at NITK Surathkal. Here's a list of all events and competitons that Incident boasts of."/>
	<?php include_once("headers.php") ?>
	<link rel="stylesheet" type="text/css" href="css/events.min.css">
	<link rel="stylesheet" type="text/css" href="css/events.media.min.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
	<script src="js/events.min.js"></script>
</head>
<body>
<?php include_once("loader.php"); ?>
<?php include_once("menu.php"); ?>
<main>
<div class="side-bar">
	<h1>Events</h1>
	<ul>
		<li class="active" data-target="#music">
			<span class="underline animate" >Musicals</span>
		</li>
		<li data-target="#dance">
			<span class="underline" >Dance</span>
		</li>
		<li data-target="#fine-arts">
			<span class="underline" >Fine Arts</span>
		</li>
		<li data-target="#lit-events">
			<span class="underline" >Lit Events</span>
		</li>
		<li data-target="#biz-events">
			<span class="underline" >Biz Events</span>
		</li>
		<li data-target="#sports">
			<span class="underline" >Sports</span>
		</li>
		<li data-target="#haute-couture">
			<span class="underline" >Haute Couture</span>
		</li>
		<li data-target="#gaming">
			<span class="underline" >Gaming</span>
		</li>
		<li data-target="#workshops">
			<span class="underline" >Workshops</span>
		</li>
		<li data-target="#specials">
			<span class="underline" >Inci Specials</span>
		</li>
		<li data-target="#beach-events">
			<span class="underline" >Beach Events</span>
		</li>
	</ul>
	<div class="scroll-bar"></div>
</div>
<div class="events">
	<div class="event" id="music">
		<img src="images/events/musicals.jpg" class="event-bg">
		<h1>Musicals</h1>
		<p class="event-description">“If I were not a physicist, I would probably be a musician. I often think in music. I live my daydreams in music. 
		I see my life in terms of music.” - Albert Einstein<br/>

		As Albus Dumbledore so famously once said, "music is a magic beyond all". This Incident prepare to have
		 your world	rocked, your socks knocked and your mouth rendered speechless. Give in to the rhythm, let the 
		 melody sweep you off your feet, may the beat never drop and may the party never stop!</p>
		 <div class="more-details">
		 	<div class="left-half">
		 		<ul>
		 			<li class="sub-event" data-target="#bandish-desc">Bandish</li>
		 			<li class="sub-event" data-target="#dhwanik-desc">Dhwanik</li>
		 			<li class="sub-event active" data-target="#pulse-desc">Pulse</li>
		 			<li class="sub-event" data-target="#unplugged-desc">Unplugged</li>
		 			<li class="sub-event" data-target="#raagalaya-desc">Raagalaya</li>
		 			<li class="sub-event" data-target="#center-stage-desc">Center Stage</li>
		 		</ul>
		 	</div>
		 	<div class="right-half">
		 		<div class="sub-event-desc" id="bandish-desc">
		 			<p class="contact">Contact</p>
		 			<h2>Bandish <label>(Semi-Professional Hindustani and Carnatic Rock Band Competition)</label></h2>
			 		<p class="desc">
			 			Thermal and Quarter. Raghu Dixit Project. Thaikkudam Bridge. Let it be remembered in the 
			 			pages of history that nobody does rock better than our desi boyz! Adorn your heart with 
			 			the tricolour at Bandish, the Eastern rock phenomenon. Because who says only the 
			 			Westerners should have all the fun?
			 		</p>
			 		<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Aeshwrya Panda<br/>
			 				<a href="tel:+917411411260">+91 74114 11260</a>
			 			</p>
			 		</div>
			 		<div>
			 			<h3 class="prizes">Prizes worth 24K + Studio Deal</h3>
				 		<a class="rules"  target="_blank" href="<?php echo $domain; ?>/files/rules/Bandish-Rules.pdf">Rules</a>
				 		<p class="date"><b>Date: </b>04/03/2017</p>
			 		</div>
		 		</div>
		 		<div class="sub-event-desc" id="dhwanik-desc">
		 			<p class="contact">Contact</p>
		 			<h2>Dhwanik <label>(Eastern Acoustic Band Competition)</label></h2>
			 		<p class="desc">
			 			This Incident, allow your soul to marvel at the wonders of Eastern music. Let the whimsical notes of the flute sweep you
						off your feet. Let the cheeky twang of the sitar snap at the strings of your heart. Let the lively beat of the tabla incite fire
						in your veins. Let Dhwanik, Incident's Eastern acoustic sensation, leave you craving more.ore.
			 		</p>
			 		<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				A F Leonard<br/>
			 				<a href="tel:+918197859387">+91 81978 59387</a>
			 			</p>
			 		</div>
			 		<div>
				 		<h3 class="prizes">Prizes worth Rs. 20,000</h3>
				 		<a class="rules"  target="_blank" href="<?php echo $domain; ?>/files/rules/Dhwanik-Rules.pdf">Rules</a>
				 		<p><b>Date: </b>03/03/2017</p>
				 	</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Dhwanik">Register</a>
		 		</div>
		 		<div class="sub-event-desc active" id="pulse-desc">
		 			<p class="contact">Contact</p>
		 			<h2>Pulse <label>(Semi-Professional Western Rock Band Competition)</label></h2>
			 		<p class="desc">
			 			Have you ever dreamt of selling out Madison Square Garden? Ever wondered how it would feel to witness a full-house
						crowd chant your name? Team Incident provides just the experience and invites you to rule the stage at Pulse, the
						Western battle of bands. We promise you a crowd unlike anything you have ever seen and will ever see! What are you
						waiting for? Register today!
			 		</p>
			 		<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Varun Siva<br/>
			 				<a href="tel:+919620830765">+91 96208 30765</a>
			 			</p>
			 		</div>
			 		<div>
			 			<h3 class="prizes">Prizes worth 24K + Studio Deal</h3>
				 		<a class="rules"  target="_blank" href="<?php echo $domain; ?>/files/rules/Pulse-Rules.pdf">Rules</a>
				 		<p><b>Date: </b>02/03/2017</p>
			 		</div>
		 		</div>
		 		<div class="sub-event-desc" id="unplugged-desc">
		 			<p class="contact">Contact</p>
		 			<h2>Unplugged <label>(Western Acoustic &amp; A cappella Band Competition)</label></h2>
			 		<p class="desc">
			 			A starry night. Gentle winds. A crackling bonfire. Waves on a beach. The strum of a guitar. Make music with your
						mouth. Mix melodies with your mind. Enthral your senses and expand your horizons. Experience contentment and
						enlightenment at Unplugged. Be it Western acoustic spectacle or an Acapella wonder, unplug your heart and let the
						music take over!
			 		</p>
			 		<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Anirudh Sriram<br/>
			 				<a href="tel:+917022090091">+91 70220 90091</a>
			 			</p>
			 		</div>
			 		<div>
			 			<h3 class="prizes">Prizes worth Rs. 20,000</h3>
				 		<a class="rules"  target="_blank" href="<?php echo $domain; ?>/files/rules/Unplugged-Rules.pdf">Rules</a>
				 		<p><b>Date: </b>03/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Unplugged">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="raagalaya-desc">
		 			<p class="contact">Contact</p>
		 			<h2>Raagalaya <label>(Solo Eastern Vocals Competition)</label></h2>
			 		<p class="desc">
			 			Ever dreamt of seeing your name among the upper echelons of maestros such as Mohd. Raffi, A.R. Rahman, Shankar
						Mahadevan and the likes? Every prodigy started as a rookie. Team Incident invites all bathroom singers and maestros
						to participate in Raagalaya, the Eastern solo competition..
			 		</p>
			 		<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Niveditha Sai<br/>
			 				<a href="tel:+919446978099">+91 94469 78098</a>
			 			</p>
			 		</div>
			 		<div>
			 			<h3 class="prizes">Prizes worth Rs. 8,000</h3>
				 		<a class="rules"  target="_blank" href="<?php echo $domain; ?>/files/rules/Raagalaya-Rules.pdf">Rules</a>
				 		<p><b>Date: </b>04/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Raagalaya">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="center-stage-desc">
		 			<p class="contact">Contact</p>
		 			<h2>Center Stage <label>(Solo Instrumental Competition)</label></h2>
			 		<p class="desc">
			 			A bright spotlight. A screaming crowd. Show time. Belt out with drums, or tone it down with the simple, effluent notes of
						a guitar! The theory is to prove that music means more than vocal cords, instruments are the equipment needed and
						the stage is your laboratory. Participate in "Centre Stage", the solo instrumental competition and prove your mettle!
			 		</p>
			 		<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Surya<br/>
			 				<a href="tel:+919611933126">+91 96119 33126</a>
			 			</p>
			 		</div>
			 		<div>
			 			<h3 class="prizes">Prizes worth Rs. 8,000</h3>
				 		<a class="rules"  target="_blank" href="<?php echo $domain; ?>/files/rules/Center-Stage-Rules.pdf">Rules</a>
				 		<p><b>Date: </b>03/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Center+Stage">Register</a>
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="event" id="dance">
		<img src="images/events/dance.jpg" class="event-bg">
		<h1>Dance</h1>
		<p class="event-description">"Dance is the hidden language of the soul of the body" A carnival is never complete without the blend of fun and 
		excitement. So to pump it up and take it a level higher, we supplement it with dance. With a plethora of dance events rounding it up, it
		 certainly promises to be one epic roller coaster ride spread across 4 days.Witness the perfect dose of dance as Promenade, Step Up and 
		 Tandav will leave you spellbound. So let's give it up for those fabulous moves to those beats out there!</p>
		 <div class="more-details">
		 	<div class="left-half">
		 		<ul>
		 			<li class="sub-event" data-target="#step-up-solo-desc">Step Up (Solo)</li>
		 			<li class="sub-event" data-target="#step-up-duet-desc">Step Up (Duet)</li>
		 			<li class="sub-event active" data-target="#promenade-desc">Promenade</li>
		 			<li class="sub-event" data-target="#thandav-desc">Tandav</li>
		 		</ul>
		 	</div>
		 	<div class="right-half">
		 		<div class="sub-event-desc" id="step-up-solo-desc">
		 			<p class="contact">Contact</p>
		 			<h2>Step Up (Solo)</h2>
			 		<p class="desc">
			 			Step up the volume, step up the beats, step up the electrifying mood, step up those dance moves. Incident gives you a 
			 			chance to take your talent one step higher with Step Up, the solo dance contest. So are you geared up to step up?
			 			 Gyrate with your moves and prove it then.
			 		</p>
			 		<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Rohit Kumar<br/>
			 				<a href="tel:+919972393575">+91 99723 93575</a>
			 			</p>
			 		</div>
			 		<div>
			 			<h3 class="prizes">Prizes worth Rs. 8,000</h3>
				 		<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/Step-Up-Solo-Rules.pdf">Rules</a>
				 		<p><b>Date: </b>04/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Step+Up+-+Solo">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="step-up-duet-desc">
		 			<p class="contact">Contact</p>
		 			<h2>Step Up (Duet)</h2>
			 		<p class="desc">
			 			What’s better than watching a live story, enacted by two people, shaping up with that eye catching choreography? Let the two 
			 			speak with their moves as Incident presents to you the duet version of Step Up where you step up the level with double the talent.
			 			 So let the double magic come across! 
			 		</p>
			 		<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Abhilasha Jagadeesh<br/>
			 				<a href="tel:+918762126955">+91 87621 26955</a>
			 			</p>
			 		</div>
			 		<div>
			 			<h3 class="prizes">Prizes worth Rs. 12,000</h3>
				 		<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/Step-Up-Duet-Rules.pdf">Rules</a>
				 		<p><b>Date: </b>04/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Step+Up+-+Duet">Register</a>
		 		</div>
		 		<div class="sub-event-desc active" id="promenade-desc">
		 			<p class="contact">Contact</p>
		 			<h2>Promenade <label>(Semi-professional Western Group Dance Competition)</label></h2>
			 		<p class="desc">
			 			A leisurely stroll would seem so relieving, but what if you get to see your favourite dance moves creating it’s
			 			 magic outside the dance floor? Presenting to you, the group Street dance competition, Promenade. So does your group 
			 			 have it to turn eyes around and make it an amazing dance Promenade? Join in and find out yourself. 
			 		</p>
			 		<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Akshay Laila<br/>
			 				<a href="tel:+917760596626">+91 77605 96626</a>
			 			</p>
			 		</div>
			 		<div>
			 			<h3 class="prizes">Prizes worth Rs. 48,000</h3>
				 		<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/Promenade-Rules.pdf">Rules</a>
				 		<p><b>Date: </b>04/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Promenade">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="thandav-desc">
		 			<p class="contact">Contact</p>
		 			<h2>Tandav <label>(Semi-professional Eastern and Contemporary Group Dance Competition)</label></h2>
			 		<p class="desc">
			 			Diversity defines India. So get ready to define this with your dancing talent as we present to you the classical 
			 			Indian dance competition, Tandav. Come, unite the East, West, North and the South on one stage with that stupendous
			 			 choreography and blend these Indian dance forms up into a wonderful story for the world to witness. So can you blend 
			 			 the mixes well? 
			 		</p>
			 		<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Gopika Mohandas<br/>
			 				<a href="tel:+918281985860">+91 82819 85860</a>
			 			</p>
			 		</div>
			 		<div>
			 			<h3 class="prizes">Prizes worth Rs. 48,000</h3>
				 		<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/Tandav-Rules.pdf">Rules</a>
				 		<p><b>Date: </b>05/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Tandav">Register</a>
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="event" id="fine-arts">
		<img src="images/events/fine-arts.jpg" class="event-bg">
		<h1>Fine Arts</h1>
		<p class="event-description">"Learn as if you were to live forever, challenge yourself as if you were to live only today." Fine arts
		 brings forth learning unique skills and also putting them to test in the most fun ways possible. Gear up, as this Incident is the time, 
		 to sharpen all your creative edges and mould your artistic side to perfection.</p>
		 <div class="more-details">
		 	<div class="left-half">
		 		<ul>
		 			<li class="sub-event active" data-target="#body-art-desc">Body Art</li>
		 			<li class="sub-event" data-target="#newspaper-costume-desc">Newspaper Costume Design</li>
		 		</ul>
		 	</div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active" id="body-art-desc">
		 			<p class="contact">Contact</p>
		 			<h2>Body Art</h2>
			 		<p class="desc">
			 			Get ready to turn your partners into canvas and show case your expertise by painting them into realistic illustrations based
			 			on a spot theme!
			 		</p>
			 		<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Navita Singhal<br/>
			 				<a href="tel:+919481655181">+91 94816 55181</a>
			 			</p>
			 		</div>
			 		<div>
			 			<h3 class="prizes">Prizes worth Rs. 3,000</h3>
				 		<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/Body-Art-Rules.pdf">Rules</a>
				 		<p><b>Date: </b>02/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Body+Art">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="newspaper-costume-desc">
		 			<p class="contact">Contact</p>
		 			<h2>Newspaper Costume Design</h2>
			 		<p class="desc">
			 			Do you think you have the knack of fashion designing? Then take this challenge of styling your partner by remodeling
			 			newspaper into iconic party wear and watch the newspaper clad  mannequins walk the ram.
			 		</p>
			 		<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Vishakha<br/>
			 				<a href="tel:+918722120531">+91 87221 20531</a>
			 			</p>
			 		</div>
			 		<div>
			 			<h3 class="prizes">Prizes worth Rs. 3,000</h3>
				 		<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/Newspaper-Costume-Design-Rules.pdf">Rules</a>
				 		<p><b>Date: </b>03/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Newspaper+Costume+Design">Register</a>
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="event" id="lit-events">
		<img src="images/events/lit-events.jpg" class="event-bg">
		<h1>Lit Events</h1>
		<p class="event-description">What's a carnival without a battle of minds? What's a festival without a confluence of thoughts?
		 Along with all the dancing and singing and madness the sharpest and the wittiest will find their own euphoria at the Lit Events.
		  Here's to the writers and thinkers and speakers in you. Come along and have a jolly ride! </p>
		 <div class="more-details">
		 	<div class="left-half">
		 		<ul>
		 			<li class="sub-event active" data-target="#jam-desc">Just A Minute</li>
		 			<li class="sub-event" data-target="#general-quiz-desc">General Quiz</li>
		 			<li class="sub-event" data-target="#india-quiz-desc">India Quiz</li>
		 			<li class="sub-event" data-target="#sports-entertainment-quiz-desc">Sports Entertainment Quiz</li>
		 			<li class="sub-event" data-target="#lone-wolf-quiz-desc">Lone Wolf Quiz</li>
		 		</ul>
		 	</div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active" id="jam-desc">
		 			<h2>Jam</h2>
			 		<p>
			 			Can you conjure up words and sentences under tremendous pressure, outwit your opponents and build an argument from absolutely 
			 			nothing? It's time to show your mettle, with quick fire thinking and lightning fast reflexes. Don’t lose out on a chance to rise 
			 			to fame in 60 seconds. You might just live the best minute of your life at Just-A-Minute.
			 		</p>
			 		<div>
			 			<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/Just-A-Minute-Rules.pdf">Rules</a>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Jam">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="general-quiz-desc">
		 			<h2>General Quiz</h2>
			 		<p>
			 			The enthusiastic applause from a roaring crowd, the buzzing of the electrified 
			 			air of suspense and the almost audible focus of the quizzers rapt on their feet.
			 			 Questions fired and answers spat back with equal zeal.This is the battlefield 
			 			 where only knowledge reigns supreme. Charge those neurons and see if your team
			 			  has got what it takes to win the general quiz.Conducted by Lord Arul Mani 
			 			  himself, this is a quiz you definitely cannot afford to miss.
			 		</p>
			 		<br/>
			 		<div>
			 			<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/General-Quiz-Rules.pdf">Rules</a>
		 				<p><b>Date: </b>05/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=General+Quiz">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="india-quiz-desc">
		 			<h2>India Quiz</h2>
			 		<p>
			 			If you are confident with your trivia about India,this is your chance.If competition 
			 			comes naturally to you,if your brain is dying for some activity and if you see thrill
			 			 in do-or-die situations,we invite you to take part in the ultimate brain storming session.
			 			 Watch while teams battle it out on the stage as answers fly back and forth with equal 
			 			 aplomb.Conducted by Arul Mani this is a quiz you definitely cannot afford to miss.
			 		</p>
			 		<br/>
			 		<div>
			 			<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/India-Quiz-Rules.pdf">Rules</a>
		 				<p><b>Date: </b>05/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=India+Quiz">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="sports-entertainment-quiz-desc">
		 			<h2>Sports &amp; Entertainment Quiz</h2>
			 		<p>
			 			Can't contain yourself with your knowledge of sports and entertainment? Come and 
			 			show all your craziness about sports and justify your love for entertainment in 
			 			one of Incident's most popular quizzes.Do you have the mettle do battle it out at 
			 			the quiz-essential zone of Incident 2017?
			 		</p>
			 		<br/>
			 		<div>
			 			<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/Sports-Entertainment-Quiz-Rules.pdf">Rules</a>
		 				<p><b>Date: </b>04/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Sports+%26+Entertainment+Quiz">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="lone-wolf-quiz-desc">
		 			<h2>Lone Wolf Quiz</h2>
			 		<p>
			 			Do you know everything from the fun facts about infinity war to the intricacies
			 			 of global economics?Are you generally shunned among your peers from being a know 
			 			 it all?(OR does information unintentionally come out blurting whenever you open
			 			  your mouth?) Well here you can actually win prizes for that! So come and
			 			   embrace the chance to show the ignoramuses that inofrmation is the real wealth!
			 		</p>
			 		<br/>
			 		<div>
			 			<a class="rules"  target="_blank" href="<?php echo $domain; ?>/files/rules/Lone-Wolf-Quiz-Rules.pdf">Rules</a>
		 				<p><b>Date: </b>04/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Lone+Wolf+Quiz">Register</a>
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="event" id="biz-events">
		<img src="images/events/biz-events.jpg" class="event-bg">
		<h1>Biz Events</h1>
		<p class="event-description">A thought. An inclination. Then a second thought. A confusion. We rack our brains. We take a calculated risk. 
		And what runs in our mind produces as a gain or a loss. Getting you onto the biz field, we bring to you Biz Events. With Mock Stock, Lobbying
		 and Biz Quiz lining up the list, it's certainly not to give a miss. Battle it out on the business field as one risk can change your game in a
		  second. So folks, are you ready to give your biz skills a chance to escalate? Then join in as your mind plays it off and you sail through.
		   With only those sharp minded risks.</p>
		 <div class="more-details">
		 	<div class="left-half">
		 		<ul>
		 			<li class="sub-event active" data-target="#mock-stock-desc">Mock Stock</li>
		 			<li class="sub-event" data-target="#mock-press-desc">Mock Press</li>
		 			<li class="sub-event" data-target="#biz-quiz-desc">Biz Quiz</li>
		 		</ul>
		 	</div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active" id="mock-stock-desc">
		 			<h2>Mock Stock</h2>
			 		<p>
			 			Want to experience the excitement of making money on the stock market? Have never known what a share is and have only wondered 
			 			how everybody buys shares and makes riches? Join the Mock Stock event of Incident ’17. You can play against the smartest of 
			 			investors and make millions. So this Inci, trade your stocks right away and be a millionaire!
			 		</p>
			 		<br/>
			 		<div>
		 				<p>
		 					<b>Date: </b>02/03/2017 - 03/03/2017<br/>
		 					<b>Venue: </b>Online Event
		 				</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Mock+Stock">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="mock-press-desc">
		 			<h2>Mock Press</h2>
			 		<p>
			 		Ever wondered how to do the act of attempting to influence the actions, policies, or decisions of officials in a government?
			 		Become a professional lobbyist this Inci! Why wait? Structure your thoughts &amp; influence the Government!
			 		</p>
			 		<br/>
			 		<div>
			 			<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/Mock-Press-Rules.pdf">Rules</a>
		 				<p>
		 					<b>Date: </b>05/03/2017<br/>
		 					<b>Time: </b>9:00 am<br/>
		 					<b>Venue: </b>Pavillion, Main Building
		 				</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Mock+Press">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="biz-quiz-desc">
		 			<h2>Biz Quiz</h2>
			 		<p>
			 			Do people often claim that you are such a know-all? Well then, prove them right. Inci BizQuiz tries to bring out your 
			 			knowledge treasure trove. Compete with the sharpest and fastest minds with a passion for the business world. Do you have 
			 			the mettle to battle it out at the quiz-essential zone of Incident '17?
			 		</p>
			 		<br/>
			 		<div>
			 			<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/Biz-Quiz-Rules.pdf">Rules</a>
		 				<p>
		 					<b>Date: </b>04/03/2017<br/>
		 					<b>Time: </b>9:30 am<br/>
		 					<b>Venue: </b>MBA Seminar Hall, SOM
		 				</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Biz+Quiz">Register</a>
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="event" id="sports">
		<img src="images/events/slamdunk.jpg" class="event-bg">
		<h1>Sports</h1>
		<p class="event-description"></p>
		 <div class="more-details">
		 	<div class="left-half">
		 		<ul>
		 			<li class="sub-event active" data-target="#slam-dunk">Slam Dunk</li>
		 			<li class="sub-event" data-target="#spin-shock">SpinShock</li>
		 		</ul>
		 	</div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active" id="slam-dunk">
		 			<h2>Slam Dunk <label>(BasketBall Competition)</label></h2>
		 			<p>Be part of a thrilling event where the heart won't just pound but dribble. This year, pit
		 				your team against the best to compete for victory, pride and glory in one of Incident’s
		 				most heated events. Also, there is a skills contest involving long range shooting and 
		 				emphatic dunks open for all participants. Afterall, what’s a carnival without a contest?</p>
		 			<h2 class="prizes">Prizes worth Rs. 75,000</h2>
		 			<p><b>Date: </b>01/03/2017 - 05/03/2017</p>
		 			<a class="rules"  target="_blank" href="<?php echo $domain; ?>/files/rules/Slam-Dunk-Rules.pdf">Rules</a>
		 		</div>
		 		<div class="sub-event-desc" id="spin-shock">
		 			<p class="contact">Contact</p>
		 			<h2>SpinShock <label>(Women's ThrowBall Competition)</label></h2>
		 			<p class="desc">Enrich the festive atmosphere of our carnival with a blend of spin,precision and
		 			grace.Come grab this unique opportunity to etch your name in the sands of winning INCIDENT’s
		 			first-ever intercollegiate throwball competition.
		 			</p>
					<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Divya<br/>
			 				<a href="tel:+918197849803">+91 81978 49803</a>
			 			</p>
			 		</div>
		 			<div>
		 				<h2 class="prizes">Prizes worth Rs. 8,000</h2>
		 				<p><b>Date: </b>04/03/2017</p>
		 				<a class="rules"  target="_blank" href="<?php echo $domain; ?>/files/rules/Spin-Shock-Rules.pdf">Rules</a>
		 			</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Spin+Shock">Register</a>
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="event" id="haute-couture">
		<img src="images/events/haute_couture.jpg" class="event-bg">
		<h1>Haute Couture</h1>
		<p class="event-description">A showcase of glamour and glitz where only the elite make it to the center stage. The viewers 
		need to brace themselves to be mesmerized by the models with their diva skills, as they walk the ramp with elegance and beauty 
		unparalleled. You have one round to bring out your creativity, so make the most of it!</p>
		 <div class="more-details">
		 	<div class="left-half"></div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active">
		 			<div class="contact-desc" style="display: block">
			 			<h2>Contact</h2>
			 			<p>
			 				Nidhi Sridhar<br/>
			 				<a href="tel:+919448247547">+91 94482 47547</a>
			 			</p>
			 		</div>
		 			<div>
		 				<h2 class="prizes">Prizes worth Rs. 85,000</h2>
				 		<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/Haute-Couture-Rules.pdf">Rules</a>
				 		<p><b>Date: </b>03/03/2017</p>
		 			</div>
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="event" id="gaming">
		<img src="images/events/gaming.jpg" class="event-bg">
		<h1>Gaming</h1>
		<p class="event-description" style="background:none;"></p>
		 <div class="more-details">
		 	<div class="left-half">
		 		<ul>
		 			<li class="sub-event active" data-target="#nfs-desc">Need For Speed</li>
		 			<li class="sub-event" data-target="#fifa-desc">FIFA</li>
		 			<li class="sub-event" data-target="#mini-militia-desc">Mini Militia</li>
		 			<li class="sub-event" data-target="#cs-desc">CS GO</li>
		 			<li class="sub-event" data-target="#dota-desc">Dota2</li>
		 			<li class="sub-event" data-target="#aoe-desc">Age Of Empires 3</li>
		 		</ul>
		 	</div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active" id="nfs-desc">
		 			<h2>Need For Speed</h2>
			 		<p>
			 			Speed has never killed anyone, suddenly becoming stationary is what gets you. So keep your acceleration high, cause racers
			 			should race and cops should eat donuts, as NFS brings you the adrenaline rush this Incident 17.
					</p>
					<br/>
		 			<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=NFS">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="fifa-desc">
		 			<h2>FIFA</h2>
			 		<p>
			 		Dribble, scissor, a quick pull and push, as you send the defenders sprawling across the grass, causing the goalkeeper to break out
			 		in panicky sweat, with the frenzied fans bordering on the state of hysterical joy. Welcome to the world of FIFA, where every move
			 		is magic per se.
			 		</p>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=FIFA">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="mini-militia-desc">
		 			<h2>Mini Militia</h2>
			 		<p>
			 			Aim with precision and shoot with deftness, as you try to dodge your enemies' bullets and parallely hit them with yours. This is
			 			where you take on the role of a virtual soldier, or you could say, 'The Punisher'.
			 		</p>
			 		<br/>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Mini+Militia">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="cs-desc">
		 			<h2>Counter Strike Global Offsensive</h2>
			 		<p>
			 			Guns blazing, corpses lying around, ammo running low and it's 3 AM in the morning. Sounds too familiar? Incident '17 presents
			 			Counter-Strike, one of the most popular games in colleges!<br/>
			 			<b>Team of 5</b>
			 		</p>
			 		<br/>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Counter+Strike">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="dota-desc">
		 			<h2>Dota2</h2>
			 		<p>
			 			Incident '17 presents the action-packed multiplayer game, Defence of the Ancients. Get your buddies along with you to NITK and cherish
			 			 the game that brought you all together!<br/>
			 			<b>Team of 5</b>
			 		</p>
			 		<br/>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Dota">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="aoe-desc">
		 			<h2>Age of Empires 3</h2>
			 		<p>
			 			This classic beauty made "history" fun and has taken the world by storm since its inception in 1997. Age of Empires, the historical
			 			strategy game is back at NITK for Incident '17!<br/>
			 			<b>Team of 2</b>
			 		</p>
			 		<br/>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Age+of+Empires">Register</a>
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="event" id="workshops">
		<img src="images/events/workshop.jpg" class="event-bg">
		<h1>Workshops</h1>
		<p class="event-description">Incident isn't all about the craze of Pronites or the madness than ensues across the five day bonanza. 
		It's about indulging in the experience and finding yourself. Like always, we bring you an appetising list of workshops to nurture your body,
		 heart and mind alike, encompassing music, dance, art, fitness and more. Come along, immerse yourself, enjoy, and have the time of your life, 
		 learning from the very best in the country.</p>
		 <div class="more-details">
		 	<div class="left-half">
		 		<ul>
		 			<li class="sub-event active" data-target="#frisbee-workshop-desc">Frisbee Workshop</li>
		 			<li class="sub-event" data-target="#guitar-clinic-desc">Guitar Clinic</li>
		 			<li class="sub-event" data-target="#dance-workshop-desc">Dance Workshop</li>
		 			<li class="sub-event" data-target="#mask-making-workshop-desc">Mask Making Workshop</li>
		 			<li class="sub-event" data-target="#ttt-workshop-desc">TTT Workshop</li>
		 		</ul>
		 	</div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active" id="frisbee-workshop-desc">
		 			<h2>Frisbee Workshop</h2>
			 		<p>
			 			Oh so how many are fond of playing around with Frisbee discs? Or perhaps play a wonderful combination of discs and the ground? 
			 			Incident ’17 brings to you Frisbee Workshop, wherein you’ll be taken through a good know-how about the celebrated game. So be there 
			 			on the learning grounds! 
			 		</p>
			 		<br/>
			 		<div>
		 				<p><b>Date: </b>05/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Frisbee+Workshop">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="guitar-clinic-desc">
		 			<h2>Guitar Clinic <label>by Baiju Dharmajan</label></h2>
			 		<p>
			 			Acoustics. Strings. Frequencies. Music. A perfect blend, a perfect match. And a wonderful serenading environment.
			 			 Who wouldn’t want to make the music run through their ears? Get ready to be a part of Guitar Clinic, a workshop 
			 			 where you can have a quick run through of the melodious journey.
			 		</p>
			 		<br/>
			 		<div>
		 				<p><b>Date: </b>02/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Guitar+Clinic">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="dance-workshop-desc">
		 			<h2>Dance Workshop <label>by HHI<label></h2>
			 		<p>
			 			“One, two, three, four, right leg behind and left leg forward. Yeah there goes your step correct.”
			 			<br/><br/>
						The moves, the steps, the beats and the blend of these defines the wonder of dance. So for all the dance enthusiasts out 
						there, Incident brings in Dance Workshop, where you get to learn different Indian and international dance forms and to know 
						the wonders of choreography. 
			 		</p>
			 		<br/>
			 		<div>
		 				<p><b>Date: </b>05/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Dance+Workshop">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="mask-making-workshop-desc">
		 			<h2>Mask Making Workshop</h2>
			 		<p>
			 			Talent unmasked!<br/>
						Artists' Forum as part of Incident 2017 proudly presents Mask making workshop for an 
						illuminating session on how to make masks of all different types out of thermofoam! 
						Learn from Mr. Sudheer Kumar, an acclaimed artist from Mangalore who's expertise in 
						this domain is unparalleled. Be there to upgrade your skill set with a mixture of 
						creativity and technique. 
			 		</p>
			 		<br/>
			 		<div>
		 				<p><b>Date: </b>03/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Mask+Making+Workshop">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="ttt-workshop-desc">
		 			<h2>TTT Workshop</h2>
			 		<p>
			 			Every atom in the universe has a story to tell. The story might be buried deep within us. 
			 			This Incident get moulded by the Wordsworth's of short stories Terribly Tiny Tales! TTT is 
			 			here to mesmerize you with their incredible writing skills. Knit your story now.
			 		</p>
			 		<br/>
			 		<div>
		 				<p><b>Date: </b>04/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=TTT+Workshop">Register</a>
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="event" id="specials">
		<img src="images/events/specials.jpg" class="event-bg">
		<h1>Inci Specials</h1>
		<p class="event-description">Drumrolls! Euphoria! Hear all! These stellar events are the quintessential gemstones of every Incident. 
		They're the heart-stoppers, jaw-droppers and one's you ought to absolutely make sure you do not miss. Presenting to you, Inci Specials, 
		where the brilliance, dance and madness ensues</p>
		 <div class="more-details">
		 	<div class="left-half">
		 		<ul>
		 			<li class="sub-event active" data-target="#kalakriti-desc">Kalakriti</li>
		 			<li class="sub-event" data-target="#expose-desc">Expose</li>
		 			<li class="sub-event" data-target="#prom-desc">Prom Night</li>
		 			<li class="sub-event" data-target="#hogathon-desc">Hogathon</li>
		 			<li class="sub-event" data-target="#stunt-show-desc">Stunt Show</li>
		 			<li class="sub-event" data-target="#vintage-expo-desc">Vintage Auto Expo</li>
		 		</ul>
		 	</div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active" id="kalakriti-desc">
		 			<h2>Kalakriti</h2>
			 		<p>
			 			Lose yourself to jaw-dropping, eye-popping and magical pieces of art and craft. Art forms ranging from sketches to paintings, 
			 			origami, kirigami and other craftwork, Kalakriti is home to all. The artist in you is sure to be motivated and inspired by the 
			 			spectacular creativity packaged into this exhibition!
			 		</p>
			 		<br/>
		 		</div>
		 		<div class="sub-event-desc" id="expose-desc">
		 			<h2>Expose</h2>
			 		<p>
			 			A picture is worth a thousand words, said a great, wise man. Be wired to experience breathtaking images, thought-provoking 
			 			scenarios and some magnificent editing wonders. Treat your eyes to some iconic photography at Incident 2017’s very own Expose, 
			 			the world through the lens.
			 		</p>
			 		<br/>
		 		</div>
		 		<div class="sub-event-desc" id="prom-desc">
		 			<h2>Prom Night</h2>
			 		<p>
			 			Time comes to a standstill as hands lovingly clasp and feet tap away to the romantic sparks all across the floor. 
			 			As love makes the aura around electric, the lovebirds do make a charming sight. A night of fun and frolic, with memories
			 			everlasting.
			 		</p>
			 		<br/>
			 		<p><b>Rs. 200/- per couple.</b></p>
			 		<br/>
		 		</div>
		 		<div class="sub-event-desc" id="hogathon-desc">
		 			<h2>Hogathon</h2>
			 		<p>
			 			As the name suggests, it is a challenge which is its own reward. The rules to this mouth-watering event are simple, eat fast, eat a lot.
			 			 Feast on a variety of foodstuffs, each better than the previous one. Only the gastronomically gifted can make it to the end, may the 
			 			 food be with you.
			 		</p>
			 		<br/>
		 		</div>
		 		<div class="sub-event-desc" id="stunt-show-desc">
		 			<h2>KTM Stunt Show</h2>
			 		<p>
			 			Life is too short to be played safe. Stunt Mania showcases daredevil riders who live up to this motto. This Incident get 
			 			ready to see the tracks set on fire quite literally! With adrenaline levels hitting the roof and every moment making your
			 			 heart thump faster, this is an event that will make you wince, gasp and cheer as the riders finish their stunts unscathed 
			 			 and triumphant!
			 		</p>
			 		<br/>
		 		</div>
		 		<div class="sub-event-desc" id="vintage-expo-desc">
		 			<h2>Vintage Auto Expo</h2>
			 		<p>
			 			Like a good wine some cars just get better as they grow older! This Incident witness some of the most classic and chic cars
			 			showcased at the Vintage Auto Expo. For all the gear heads and automobile enthusiasts out there, you really most definitely
			 			do not want to miss this! Visit Incident'17 and witness the saying "old is gold" come true!
			 		</p>
			 		<br/>
		 		</div>
		 	</div>
		 </div>
	</div>
		<div class="event" id="beach-events">
		<img src="images/events/beach.jpg" class="event-bg">
		<h1>Beach Events</h1>
		<p class="event-description">Ever fantasized of the thrill of experiencing watersport adventures or imagined yourself to be part of the Baywatch 
		Squad? Incident'17 brings your ideas to life by presenting an exciting series of Beach Events including Jet Skiing, Banana Rides, Beach 
		Volleyball, Football and many more. Come bathe in the golden sands and indulge in the sportive spirit. A scintillating view of the sunset 
		over the azure waters awaits you!</p>
		 <div class="more-details">
		 	<div class="left-half">
		 		<ul>
		 			<li class="sub-event active" data-target="#dj-war-desc">DJ war</li>
		 			<li class="sub-event" data-target="#kite-festival-desc">Kite Festival</li>
		 			<li class="sub-event" data-target="#beach-informalz-desc">Beach Informals</li>
		 		</ul>
		 	</div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active" id="dj-war-desc">
		 			<h2>DJ War</h2>
			 		<p>
			 			Let the Beat drop! Lace up to jump your hearts out at Incident's DJ Wars as talents from across the
						region battle to keep the party alive in the beach. Come find yourself dancing to the rhythm of
						killer bass and experience a war like no other! The serenading waves and pearl sands of the NITK
						Beach scream out to you, Are you Ready?!
			 		</p>
			 		<div>
			 			<h3 class="prizes">Prizes worth Rs. 25,000</h3>
			 			<a class="rules" target="_blank"  href="<?php echo $domain; ?>/files/rules/War-of-DJs-Rules.pdf">Rules</a>
			 			<p><b>Date: </b> 03/03/2017</p>
			 		</div>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=DJ+War">Register</a>
		 		</div>
		 		<div class="sub-event-desc" id="kite-festival-desc">
		 			<h2>Kite Festival</h2>
			 		<p>
			 			Kite flying is a sport that brings out the little child in each one of us and lights up a smile on our face.
						As the kites fly higher and higher so does our spirit and cheerfulness. This Incident, join us as these colourful 
						creative kites take to the air and paint the sky with joy! Come, fly a kite and travel back to your childhood!
			 		</p>
			 		<br/>
			 		<p><b>Date: </b> 05/03/2017</p>
		 		</div>
		 		<div class="sub-event-desc" id="beach-informalz-desc">
		 			<h2>Beach Informalz</h2>
			 		<p>
			 			Not finding any events to your taste? Bored out of your mind? No worries! All you gotta do is take a stroll to 'The Beach'.
			 			 The Informalz is awaiting you! From mind-blowing games  to silly competitions between your buddies, the Beach Informalz
			 			 offers a dose of fun and frolic that you cannot refuse!
			 		</p>
			 		<br/>
		 		</div>
		 	</div>
		 </div>
	</div>
</div>
</main>
</body>
</html>