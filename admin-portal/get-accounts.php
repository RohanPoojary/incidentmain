<?php
  session_start();
  //error_reporting(E_ALL); ini_set('display_errors', 1);
  //$_SESSION['login'] = "set";

  if(!isset($_SESSION['login']))
  {
    $_SESSION['error'] = "Not Logged In";
    $post_data = json_encode($_SESSION);
    echo htmlentities($post_data);
    exit();
  }
 
  require_once("includes/db_connect.php");
  
  $query="SELECT * FROM accounts";

  if(isset($_GET['q']))
  {
  	$q = mysqli_real_escape_string($connection,trim($_GET['q']));
  	$query .= " WHERE account_id LIKE '%{$q}%' OR full_name LIKE '%{$q}%' OR email LIKE '%{$q}%' OR college LIKE '%{$q}%' OR mobile_number LIKE '%{$q}%' OR degree LIKE '%{$q}%'"; 
    //echo $query;
  }

  if(isset($_GET["len"])) {
    $result = mysqli_query($connection,$query);
    echo mysqli_num_rows($result);
    exit;
  }

  $query .= " ORDER BY college";

  if(isset($_GET['limit'])&&$_GET["limit"]>=0)
  {
    $limit = mysqli_real_escape_string($connection,trim($_GET['limit']));
    $query .= " LIMIT {$limit}";
  }else{
    $query .= " LIMIT 10000";
  }

  if(isset($_GET['offset'])&&$_GET["offset"]>=0)
  {
  	$offset = mysqli_real_escape_string($connection,trim($_GET['offset']));
    $query .= " OFFSET {$offset}";
  }

   
  $result = mysqli_query($connection,$query);
  $output = array();
  
  if($result && mysqli_num_rows($result)>0)
  {
    while($row = mysqli_fetch_assoc($result))
    {
      $details = array('account_id' => $row['account_id'],'full_name'=>$row['full_name'],'email'=>$row['email'],'college'=>$row['college'],'mobile_number'=>$row['mobile_number'],'degree'=>$row['degree'],'year_of_study'=>$row['year_of_study'],'image_url'=>$row['image_url']);
      array_push($output, $details);
      //secho "<pre>".htmlentities($post_data)."</pre>";
    }
     mysqli_free_result($result);

    // echo "[" . json_encode($output[0]);
    // for($i=1; $i<sizeof($output); $i++) {
    //   echo ",";
    //   echo json_encode($output[$i]);
    // }
    // echo "]";

    $post_data = json_encode($output);
    echo $post_data;
  }
?>