<?php
  require_once("credentials.php");
  
  $connection = mysqli_connect($DB_SERVER,$DB_USERNAME,$DB_PASSWORD,$DB_DATABASE);

  if(mysqli_connect_errno()){
    die("Database connection failed");
  }
?>
