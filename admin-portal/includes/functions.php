<?php

  function fieldname_as_text($fieldname)
  {
	  $fieldname = str_replace("_"," ",$fieldname);
	  $fieldname = ucfirst($fieldname);
	  return $fieldname;
  }

  function has_presence($var)
  {
    if(isset($var) && $var!="")
      return 1;
    return 0;
  }
   
  function validate_presence($require_fields)
  {  
    foreach($require_fields as $field)
	  {
	    $value = trim($_POST[$field]);
	    if(!has_presence($value))
	    {
		    $_SESSION['error'][$field]=fieldname_as_text($field)." cant be blank";
        //echo $_SESSION['error'][$field];
	    }
    }	
  }

  function has_max_length($field,$max)
   {
	   $value = trim($_POST[$field]);
	   if(strlen($value)>$max)
	   {
		   return 1;
	   }
	   else 
		   return 0;
   }

   function validate_max_length($require_fields)
   {
	   global $error;
	   foreach($require_fields as $field=>$max)
	   {
		   if(has_max_length($field,$max) && !isset($_SESSION['error'][$field]))
		   {
				$_SESSION['error'][$field]=fieldname_as_text($field)." cant exceed ".$max." characters";
          // echo $_SESSION['error'][$field];
		   }
	   }
   }
   
   function has_min_length($field,$min)
   {
	   $value = trim($_POST[$field]);
	   if(strlen($value)<$min)
	   {
		   return 1;
	   }
	   else 
		   return 0;
   }

   function validate_min_length($require_fields)
   {
	   global $error;
	   foreach($require_fields as $field=>$min)
	   {
		   if(has_min_length($field,$min) && !isset($_SESSION['error'][$field]))
		   {
				   $_SESSION['error'][$field]=fieldname_as_text($field)." cant be less than ".$min." characters";
           //echo $_SESSION['error'][$field];
		   }
	   }
   }  