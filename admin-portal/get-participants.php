<?php
  
  session_start();
 // error_reporting(E_ALL); ini_set('display_errors', 1);
 // $_SESSION['login'] = "set";

  if(!isset($_SESSION['login']))
  {
  	$_SESSION['error'] = "Not Logged In";
    $post_data = json_encode($_SESSION);
    echo htmlentities($post_data);
  	exit();
  }
 // echo $_SESSION['login'];
  require_once("includes/db_connect.php");

  function get_event($event_id)
  {
    global $connection;

    $event_id = mysqli_real_escape_string($connection,trim($event_id));
    $query = "SELECT name FROM events WHERE  id='{$event_id}'";
    $result = mysqli_query($connection,$query);
    if($result && mysqli_num_rows($result)>0)
    {
      $row = mysqli_fetch_assoc($result);
      mysqli_free_result($result);
      return $row['name'];
    }
  }

  function get_event_ids($event)
  {
    global $connection;

    $event = mysqli_real_escape_string($connection,trim($event));
    $query = "SELECT id FROM events WHERE NAME LIKE '%{$event}%'";
    $result = mysqli_query($connection,$query);
    if($result && mysqli_num_rows($result)>0)
    {
      $row = mysqli_fetch_assoc($result);
      mysqli_free_result($result);
      return $row;
    }
    else return "Empty";
  }

  if(isset($_GET["len"])) {
   $query = "SELECT DISTINCT cap_id FROM participants";
  }else {
    $query = "SELECT cap_id FROM participants";
  }

  if(isset($_GET['q']))
  {
    $q = mysqli_real_escape_string($connection,trim($_GET['q']));
    $query.=" WHERE id LIKE '%{$q}%'";
    $ids = get_event_ids($q);
    if($ids != "Empty")
    {
      $query.=" OR event_id IN (";
      foreach($ids as $id)
      {
        $query.="{$id},";
      }
      $query.="{$id})";
    }
  }

  if(isset($_GET["len"])) {
    $result = mysqli_query($connection,$query);
    echo mysqli_num_rows($result);
    exit;
  }

  $query.=" ORDER BY time_stamp ASC ";

  // echo $query;

  if(isset($_GET['limit'])&&$_GET["limit"]>=0)
  {
    $limit = mysqli_real_escape_string($connection,trim($_GET['limit']));
    $query .= " LIMIT {$limit}";
  }else{
    $query .= " LIMIT 10000";
  }

  if(isset($_GET['offset'])&&$_GET["offset"]>=0)
  {
    $offset = mysqli_real_escape_string($connection,trim($_GET['offset']));
    $query .= " OFFSET {$offset}";
  }


  $result1 = mysqli_query($connection,$query);
  $output = array();
  $caps = array();
  if($result1 && mysqli_num_rows($result1)>0)
  {
    //$output = array();
    //$caps = array();
    while($row1 = mysqli_fetch_assoc($result1))
  	{
      $details = array('cap_id'=>$row1['cap_id']);
      if(in_array(strtoupper($row1['cap_id']), $caps, true)) {
        // echo "Exists". $row1['cap_id'] . "<br/><br/>";
        continue;
      }
      array_push($caps, strtoupper($row1['cap_id']));
      $query = "SELECT DISTINCT event_id, team FROM participants WHERE cap_id='".$row1['cap_id']."'";
      $result2 = mysqli_query($connection,$query);
      // print_r($result2);
      if($result2 && mysqli_num_rows($result2)>0)
      {
      	$details['events'] = array();
        $details['participants'] = array();
        $details['teams'] = array();
        $i=0;
        while($row2 = mysqli_fetch_assoc($result2))
        {
        	$event = get_event($row2['event_id']);
        	array_push($details['events'],$event);
          array_push($details['teams'], $row2['team']);

        	$query = "SELECT id FROM participants WHERE event_id='".$row2['event_id']."' AND cap_id='".$row1['cap_id']."'";
          $result3 = mysqli_query($connection,$query);           
            
          $details['participants'][$i] = array();
          while($row3 = mysqli_fetch_assoc($result3))
          {
            array_push($details['participants'][$i], $row3['id']);
          }
          mysqli_free_result($result3);
          $i+=1;
        }
        mysqli_free_result($result2);
      }
      $query = "SELECT id,SUM(day1),SUM(day2),SUM(day3),SUM(day4),SUM(day5) FROM accomodation WHERE id='".$row1['cap_id']."'";
      $result2 = mysqli_query($connection,$query);

      if($result2 && mysqli_num_rows($result2)>0)
      {
        $details['accomodation'] = array();
        $row2 = mysqli_fetch_assoc($result2);
        array_push($details['accomodation'],$row2['SUM(day1)']);
        array_push($details['accomodation'],$row2['SUM(day2)']);
        array_push($details['accomodation'],$row2['SUM(day3)']);
        array_push($details['accomodation'],$row2['SUM(day4)']);
        array_push($details['accomodation'],$row2['SUM(day5)']);
        mysqli_free_result($result2);
      }
      
      array_push($output, $details);
      unset($details);
  	}
    mysqli_free_result($result1);
  }
  
  $query = "SELECT id,SUM(day1),SUM(day2),SUM(day3),SUM(day4),SUM(day5) FROM accomodation";
  if(!empty($caps))
  {
    $query.=" WHERE id NOT IN (";
    foreach($caps as $id)
    {
      $query.="'{$id}',";
    }
    $query.="'{$id}')";
  }
  $query .= " GROUP BY id";

  $result = mysqli_query($connection,$query);
  if($result && mysqli_num_rows($result)>0)
  {   
    while($row = mysqli_fetch_assoc($result))
    {
      $details = array();
      $details['cap_id'] = strtoupper($row['id']);
      $details['accomodation'] = array();
      array_push($details['accomodation'],$row['SUM(day1)']);
      array_push($details['accomodation'],$row['SUM(day2)']);
      array_push($details['accomodation'],$row['SUM(day3)']);
      array_push($details['accomodation'],$row['SUM(day4)']);
      array_push($details['accomodation'],$row['SUM(day5)']);
      array_push($output, $details);
      unset($details);
    }
  }


  echo json_encode($output);

?>