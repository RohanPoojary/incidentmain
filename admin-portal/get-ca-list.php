<?php
  session_start();
  //error_reporting(E_ALL); ini_set('display_errors', 1);
  //$_SESSION['login'] = "set";

  if(!isset($_SESSION['login']))
  {
    $_SESSION['error'] = "Not Logged In";
    $post_data = json_encode($_SESSION);
    echo htmlentities($post_data);
    exit();
  }
 
  require_once("includes/db_connect.php");
  
  $query="SELECT * FROM registered_campus_ambassador";

  if(isset($_GET['q']))
  {
  	$q = mysqli_real_escape_string($connection,trim($_GET['q']));
  	$query .= " WHERE serial_no LIKE '%{$q}%' OR full_name LIKE '%{$q}%' OR email LIKE '%{$q}%' OR college LIKE '%{$q}%' OR degree LIKE '%{$q}%' OR mobile_number LIKE '%{$q}%' OR residential_address LIKE '%{$q}%'"; 
    //echo $query;
  }

  if(isset($_GET["len"])) {
    $result = mysqli_query($connection,$query);
    echo mysqli_num_rows($result);
    exit;
  }

  $query .=" ORDER BY serial_no ASC";

  if(isset($_GET['limit'])&&$_GET["limit"]>=0)
  {
    $limit = mysqli_real_escape_string($connection,trim($_GET['limit']));
    $query .= " LIMIT {$limit}";
  }else{
    $query .= " LIMIT 10000";
  }

  if(isset($_GET['offset'])&&$_GET["offset"]>=0)
  {
  	$offset = mysqli_real_escape_string($connection,trim($_GET['offset']));
    $query .= " OFFSET {$offset}";
  }

   
  $result = mysqli_query($connection,$query);
  
  if($result && mysqli_num_rows($result)>0)
  {
    $output = array();
    while($row = mysqli_fetch_assoc($result))
    {
      $details = array('serial_no' => $row['serial_no'],'full_name'=>$row['full_name'],'email'=>$row['email'],'college'=>$row['college'],'mobile_number'=>$row['mobile_number'],'degree'=>$row['degree'],'year_of_study'=>$row['year_of_study']);
      array_push($output, $details);
    }
    mysqli_free_result($result);
    $post_data = json_encode($output);
    echo $post_data;
  }
?>