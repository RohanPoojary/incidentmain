<?php
	session_start();
	require_once("includes/credentials.php");
	require_once("includes/functions.php");

	if(isset($_SESSION['login'])) {
		header("location: details.php");
		exit;
	}

	unset($_SESSION["error"]);
	error_reporting(E_ALL); ini_set('display_errors', 0);
	if(isset($_POST['Login'])) {
		$required_fields = array("username","password");
		validate_presence($required_fields);
		$max_lengths = array("username"=>15,"password"=>15);
		validate_max_length($max_lengths);
		$min_lengths = array("username"=>3,"password"=>3);
		validate_min_length($min_lengths);

		if(!isset($_SESSION["error"])) {
			$post_user = trim($_POST['username']);
			$post_pass = trim($_POST['password']);

			if($username==$post_user && $password==$post_pass) {
				$_SESSION['login'] = "set";
				header('Location: details.php');
				exit;
			}
			else if($username2==$post_user && $password2==$post_pass){
				$_SESSION['login'] = "set";
				$_SESSION['user'] = "set";
				header('Location: details.php');
				exit;
			}
			else{
				$_SESSION['error']['login'] = true;
			}
		} 
		else {
			$_SESSION["error"] = "Wrong Login Credentials";
			$post_data = json_encode($_SESSION);
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Incident 2017 | Admin</title>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta charset="utf-8"/>
	<meta name="robots" content="NOFOLLOW, NOINDEX"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="../css/admin.min.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
</head>
<body class="indigo lighten-5">
	<div class="container">
		<div class="img-wrapper">
			<img class="responsive-img" src="<?php echo $domain;?>/images/Logo.png">
		</div>
		<h4 class="center-align">Admin Panel</h4>
		<?php
			if(isset($_SESSION["error"])) { ?>
				<p class="red-text center-align"><b>Wrong Username or Password</b></p>
			<?php }
		 ?>
		<div class="row">
			<form method="POST" action="index.php" class="col s12 m6 offset-m3">
				<div class="row">
					<div class="input-field col s12">
						<input type="text" name="username" id="username" required/>
						<label for="username">Username</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input type="password" name="password" id="password" required/>
						<label for="password">Password</label>
					</div>
				</div>
				<div class="row center-align">
					<button class="btn" name="Login" type="submit">Login</button>
				</div>
			</form>
		</div>
	</div>
</body>
</html>