<?php
 session_start();
 require_once("includes/credentials.php");

 if(!isset($_SESSION['login'])) {
		header("location: index.php");
		exit;
 }

 $isadmin = true;

 if(isset($_SESSION['user'])) {
 	$isadmin=false;
 }

?>
<!DOCTYPE html>
<html ng-app="incidentApp">
<head>
	<title>Incident 2017 | Admin Details</title>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta charset="utf-8"/>
	<meta name="robots" content="NOFOLLOW, NOINDEX"/>
	<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/admin.min.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
</head>
<body class="indigo lighten-5">
<main ng-controller="accountsController as accCtrl">
	<div class="fixed-action-btn">
	<a href="logout.php" class="btn-floating btn-large red">
		<i class="large material-icons">input</i>
	</a>
    </div>
	<div class="img-wrapper small-img">
			<img class="responsive-img" src="<?php echo $domain;?>/images/Logo.png">
	</div>
	<h1 class="indigo-text text-darken-3 center-align">Incident 2017</h1>
	<div class="tabs-wrapper white">	
		<div class="row">
			<div class="col s12 zero-padding">
				<ul class="tabs indigo">
				<?php 
				if($isadmin) {
					?>
					<li class="tab col s3 indigo-text"><a ng-click="accCtrl.setTab(0)" href="#accounts">Accounts</a></li>
				<?php
					}
				?>
					<li class="tab col s3 indigo-text"><a ng-click="accCtrl.setTab(1)" class="active" href="#accounts" >Participants</a></li>
				<?php 
				if($isadmin) {
					?>
					<li class="tab col s3 indigo-text"><a ng-click="accCtrl.setTab(2)" href="#accounts">CA</a></li>
				<?php
					}
				?>
				</ul>
			</div>
			<div id="accounts">
				<nav class="indigo row">
					<div class="indigo col s12">
						<div class="col m1 s6 center-align">
							<a ng-click="accCtrl.refresh()" class="indigo lighten-3 waves-light btn-floating waves-effect"><i class="material-icons">autorenew</i></a>
						</div>
						<div class="col m1 s6 center-align">
							<a ng-click="accCtrl.download()" class="indigo lighten-3 waves-light btn-floating waves-effect"><i class="material-icons">get_app</i></a>
						</div>
						<div class="col m1 s4">
							Items/Page:
						</div>
						<div class="col m1 s8 input-field">
							<select onchange="setFirstPage()" ng-model="accCtrl.pages.rate" ng-options="option for option in accCtrl.pages.options">
							</select>
						</div>
						<ul class="pagination col m3 s12">
							<li ng-hide="accCtrl.pages.isActive('first')" class="waves-effect"><a href="#!" ng-click="accCtrl.pages.setActive('prev')"><i class="material-icons">chevron_left</i></a></li>
							<li class="page-options" ng-class="{active: accCtrl.pages.isActive(page)}" ng-repeat="page in accCtrl.pages.indices"><a ng-click="accCtrl.pages.setActive(page)">{{ page }}</a></li>
							<li ng-hide="accCtrl.pages.isActive('last')" class="waves-effect"><a href="#!" ng-click="accCtrl.pages.setActive('next')"><i class="material-icons">chevron_right</i></a></li>
						</ul>
						<div class="search-bar col m4 s12">
							<form name="searchBar">
								<div class="input-field">
									<input id="search" type="search" name="searchValue" ng-model="accCtrl.searchValue">
									<label for="search"><i class="material-icons">search</i></label>
									<i ng-click="accCtrl.initSearch()" class="material-icons">close</i>
								</div>
							</form>
						</div>
						<button ng-click="accCtrl.setSearch()" class="btn col m1 s8 offset-s2">Search</button>
					</div>
				</nav>
				<div class="col s12">
					<div ng-show="accCtrl.loading" class="progress">
					    <div class="indeterminate"></div>
					</div>
					<?php 
					if($isadmin) {
					?>
					<table ng-if="!accCtrl.isTab(1)" ng-hide="accCtrl.loading" class="striped centered responsive-table">
						<thead>
							<tr>
								<th>No.</th>
								<th ng-repeat="header in accCtrl.headers">{{ header[0] }}</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="account in accCtrl.pages.getAccounts()">
								<td>
									{{	$index + 1 }}
								</td>
								<td ng-repeat="header in accCtrl.headers">
									<a ng-if="accCtrl.isLink(header[1])" ng-href="accounts.php?id={{ account[header[1]] }}">{{ account[header[1]] }}</a>
									<span ng-if="!accCtrl.isLink(header[1])">{{ account[header[1]] }}</span>
								</td>
							</tr>
						</tbody>
					</table>
					<?php
						}
					?>
					<table ng-if="accCtrl.isTab(1)" ng-hide="accCtrl.loading" class="striped centered responsive-table">
						<thead>
							<tr>
								<th>No.</th>
								<th ng-repeat="header in accCtrl.headers">{{ header[0] }}</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="account in accCtrl.pages.getAccounts()">
								<td>
									{{	$index + 1 }}
								</td>
								<td>
									<a ng-href="accounts.php?id={{ account.cap_id }}">{{ account.cap_id }}</a>
								</td>
								<td>
									<div class="flexbox">
										<div class="flex-element" ng-repeat="event in account.events">{{ event }}</div>
									</div>
								</td>
								<td>
									<div class="flexbox">
										<div class="flex-element" ng-repeat="team in account.events">{{ account.teams[$index] }}</div>
									</div>
								</td>
								<td>
									<div class="flexbox">
										<div class="flex-element" ng-repeat="event_part in account.participants">
											<a ng-href="accounts.php?id={{ participant }}" ng-repeat="participant in event_part">{{ participant }}  </a>
										</div>
									</div>
								</td>
								<td>
									<div class="flexbox">
										<div>
											<span ng-repeat="index in [0, 1, 2, 3, 4]">{{ account.accomodation[index] }}  </span>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<script src="../js/admin.angular.min.js"></script>
</main>
</body>
</html>