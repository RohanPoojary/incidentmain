<?php
 session_start();
 require_once("includes/credentials.php");

 if(!isset($_SESSION['login'])) {
		header("location: index.php");
		exit;
 }

?>
<!DOCTYPE html>
<html ng-app="accountApp">
<head>
	<title>Incident Portal | Get ID</title>
  	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta charset="utf-8"/>
	<meta name="robots" content="NOFOLLOW, NOINDEX"/>
	<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/admin.min.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>

</head>
<body class="indigo lighten-5">
<main ng-controller="accountController as accCtrl">
	<div class="container">
		<div class="row">
			<div class="col s12 m6 offset-m3">
				<div class="card large">
					<div class="card-image">
						<img ng-src="{{ accCtrl.account.image_url }}">
						<span class="card-title">{{ accCtrl.account.full_name }}</span>
					</div>
					<div class="card-content">
						<img class="bg" src="<?php echo $domain;?>/images/Logo-text.png" />
						<p class="card-id">ID: {{ accCtrl.account.account_id }}</p>
						<div class="row">
							<div class="col s12 m6"><label>Name:</label>{{ accCtrl.account.full_name }}</div>
							<div class="col s12 m6"><label>College:</label>{{ accCtrl.account.college }}</div>
						</div>
						<div class="row">
							<div class="col s12 m6"><label>Email:</label>{{ accCtrl.account.email }}</div>
							<div class="col s12 m6"><label>Degree:</label>{{ accCtrl.account.degree }} </div>
						</div>
						<div class="row">
							<div class="col s12 m6"><label>Mobile No:</label>{{ accCtrl.account.mobile_number }}</div>
						</div>
					</div>
				</div>
       		<a class="btn" href="details.php">Go Back</a>
			</div>
		</div>
	</div>
</main>
<script src="../js/admin.angular.min.js"></script>
</body>
</html>