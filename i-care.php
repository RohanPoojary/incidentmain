<!DOCTYPE html>
<html>
<head>
	<title>I-Care | Incident 2017</title>
	<meta name="description" content="I-Care is the social initiative of Incident. This edition of I-Care strives to provide education to the deprived."/>
	<?php include_once("headers.php") ?>
	<link rel="stylesheet" type="text/css" href="css/events.min.css">
	<link rel="stylesheet" type="text/css" href="css/events.media.min.css">
	<link rel="stylesheet" type="text/css" href="css/i-care.min.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
	<script src="js/events.min.js"></script>
</head>
<body>
<?php include_once("loader.php"); ?>
<?php include_once("menu.php"); ?>
<main>
<div class="side-bar">
	<h2>I-Care</h2>
	<ul>
		<li class="active" data-target="#beach-schooling">
			<span class="underline animate">Beach Schooling</span>
		</li>
		<li data-target="#solar-lamps">
			<span class="underline">Solar Lamps</span>
		</li>
		<li data-target="#school-day">
			<span class="underline">School Day</span>
		</li>
	</ul>
	<div class="scroll-bar"></div>
</div>
<div class="events">
	<div class="event" id="beach-schooling">
		<img src="images/events/beach-schooling.jpg" class="event-bg">
		<h1>Beach Schooling</h1>
		<p class="event-description">
			As the name suggests, Beach Schooling is a gurukul on the Beach! This is a program, wherein NITK
			 students volunteer to interact with these under-privileged kids every day. In this modern gurukul, 
			 students are taught not under the banyan tree, but on the NITK Beach amidst refreshing air, the
			  resounding glory of temple bells and the dancing waves of the Arabian Sea. Currently, the program 
			  is conducted in the backyards of the famous Sadashiva Temple in Surathkal, Mangaluru. Incident,NITK
			   Surathkal is proud of the fact that Beach Schooling is the first program of its kind taken up by 
			   college students in India.</p>
	</div>
	<div class="event" id="solar-lamps">
		<img src="images/events/solar-lamps.jpg" class="event-bg">
		<h1>Solar Lamps</h1>
		<p class="event-description">
			In order to tackle power outages in the summer, solar lamps were distributed to poor kids 
			around NITK Surathkal to light up their houses. 14 solar lamps were distributed to 
			underprivileged children as a part of I-Care's agenda to promote child education and 
			35 more lamps were distributed to all the 10th standard students, who have their board 
			exams in the month of March. I-Care believes that while every student receives education,
			 it is equally important to have a conductive atmosphere at home which allows them to study 
			 and helps them in their overall growth.</p>
	</div>
	<div class="event" id="school-day">
		<img src="images/events/school-day.jpg" class="event-bg">
		<h1>School Day</h1>
		<p class="event-description">
			Along with studies, even co-curricular activites contribute a major role in development of a
			student, with this motive I-Care Team had origanized Scool Day in Thadambail Panchayat School, whose kids were deprived of
			School Day since many years due to lack of funds.
		</p>
	</div>
</div>
</main>
</body>
</html>