	<?php include_once("includes/domain.php");?>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta charset="utf-8"/>
	
	<link rel="icon" href="<?php echo $domain;?>/images/Logo.png">
	<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo $domain;?>/css/loader.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $domain;?>/css/01-menu.min.css">
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="<?php echo $domain;?>/js/loader.min.js"></script>
	<script async src="<?php echo $domain;?>/js/menu.min.js"></script>