<!DOCTYPE html>
<html>
<head>
	<title>Need Help | Incident - NITK Surathkal</title>
	<?php include_once("../headers.php"); ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="../css/portal-index.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
</head>
<body>
<?php include_once("../loader.php"); ?>
<?php include_once("../menu.php"); ?>
<main>
<div class="container">
	<div class="img-wrapper">
		<img class="responsive-img" src="<?php echo $domain;?>/images/Logo.png">
	</div>
	<h5 class="center-align purple-text text-darken-4">Frequently Asked Questions</h5>
	<br/>
	<ul class="collapsible popout" data-collapsible="accordion">
		<li>
			<div class="collapsible-header">
				How do I register for Incident?
			</div>
			<div class="collapsible-body">'
				<p>Go to the Register tab and create an account using facebook or google+ to get your Inci ID.</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				Is there a registration free?
			</div>
			<div class="collapsible-body">
				<p>Yes. The registration fee is Rs 150/- per person and they can take part in any number of 
				events. Concerts and pro-shows will be charged seperately.</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				How do I register for a competition?
			</div>
			<div class="collapsible-body">
				<p>If you do not have Inci ID then goto the registration tab and register yourself there to get
				 an Inci ID.Then go to the events tab and use that inci ID to register for any event. If you already
				 have an Inci ID then directly register for the event by clicking on the respective competition name.</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				Are there any on spot registration allowed?
			</div>
			<div class="collapsible-body">'
				<p>On spot registration are subject to already registered participants.</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				What will be the venues/time of our competitions?
			</div>
			<div class="collapsible-body">'
				<p>Schedule and venue will be updated on the website and also posted on our facebook page. 
				Please follow our page to get regular updates about the same.Also, on entering the campus
				 during the festival you will be handed a schedule of the competitions along with their venues.</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				How can I attend the proshows?
			</div>
			<div class="collapsible-body">'
				<p>Tickets will be available on book my show. Also tickets can be bought on the day of the show.</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				What type of accommodation will be provided?
			</div>
			<div class="collapsible-body">'
				<p>Accommodation will be provided to boys and girls seperately in well secured residential complexes on campus.</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				Does accommodation fee include the food facility as well?
			</div>
			<div class="collapsible-body">'
				<p>No. The accommodation charges don’t include food. However, there will be food courts 
				operational during Incident to cater to the food requirements.</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				What are the accommodation charges?
			</div>
			<div class="collapsible-body">'
				<p>It is Rs 150/- per day(Non-refundable). Payable only at the registration desk, during the fest.</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				Once I register online for accomodation, is my accommodation confirmed?
			</div>
			<div class="collapsible-body">'
				<p> Everyone who has registered online for accomodation will be accomodated in the campus by our
				 team, for sure. The ones who do not register online will be accomodated based on the availability
				 of rooms.</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				We are a group of friends not participating in any of the major competitions and just coming to 
				Incident to have fun. Would we be allowed?
			</div>
			<div class="collapsible-body">'
				<p>Yes, you can definitely attend the fest by registering from your respective college. </p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				What is the procedure to be followed after we reach NITK?
			</div>
			<div class="collapsible-body">'
				<p>You need to come to the Registration Desk in the Main Building along with your college 
				identity card. You will be allotted your place of stay on campus once the payment is done 
				and given a registration booklet.</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				Will there be any transportation facility provided by your team for us to reach your campus from the railway station / airport?
			</div>
			<div class="collapsible-body">'
				<p>No. Incident team will not be providing any team with transportation facility.</p>
			</div>
		</li>
  </ul>
  <br/>
  <div class="center-align purple-text text-darken-4">
  	<h4>Not Solved?</h4>
  	<p class="contact">Please contact <a href="<?php echo $domain; ?>/contact-us.php">OUR TEAM</a></p>
  </div>
</div>
</main>
</body>
</html>