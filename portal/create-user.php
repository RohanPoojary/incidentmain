<?php 
session_start(); 
if(!isset($_SESSION['state'])) {
	header("location: index.php");
	exit;
}
// $_SESSION['state'] = "11";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Incident Portal | Create User</title>
	<?php include_once("../headers.php"); ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="../css/portal-index.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
	<script src="../js/01-portal.min.js"></script>
</head>
<body>
<?php include_once("../loader.php"); ?>
<?php include_once("../menu.php"); ?>
<main>
	<div class="container">
		<div class="img-wrapper small-img">
			<img class="responsive-img" src="<?php echo $domain;?>/images/Logo.png">
		</div>
		<h4 class="center-align">Create Account</h4>
		<p class="center red-text"><b>Fill NA if Not Applicable</b></p>
		<div class="form-wrapper form-large">
		<?php
			if($_SESSION['state'] == "11" || ($_SESSION['state'] == "-12" && !isset($_SESSION['error']['database'])))
			{
		?>
			<form method="POST" action="create-user-complete.php">
			<div class="row">
				<div class="input-field col m6 s12">
					<input required readonly value="<?php echo htmlentities($_SESSION['full_name']); ?>" name="Full_Name" type="text">
					<label for="Full_Name">Name</label>
					<div class="error">
						<?php if(isset($_SESSION['error']['Full_Name'])) echo $_SESSION['error']['Full_Name']; ?>
					</div>
				</div>
				<div class="input-field col m6 s12">
			        <input required readonly value="<?php echo htmlentities($_SESSION['email']); ?>" name="Email" type="text">
					<label for="Email">Email</label>
					<div class="error">
						<?php if(isset($_SESSION['error']['Email'])) echo $_SESSION['error']['Email']; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="input-field col m6 s12">
					<input required name="Degree" type="text"/>
					<label for="Degree">Degree</label>
					<div class="error">
						<?php if(isset($_SESSION['error']['Degree'])) echo $_SESSION['error']['Degree']; ?>
					</div>
				</div>
				<div class="input-field col m6 s12">
					<select name="Year_Of_Study">
						<option value="1">First</option>
						<option value="2">Second</option>
						<option value="3">Third</option>
						<option value="4">Fourth</option>
						<option value="5">Fifth</option>
						<option value="NA">Not Applicable</option>
			        </select>
					<label for="Year_Of_Study">Year Of Study</label>
					<div class="error">
						<?php if(isset($_SESSION['error']['Year_Of_Study'])) echo $_SESSION['error']['Year_Of_Study']; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="input-field col m6 s12">
					<input required name="College" type="text">
					<label for="College">College</label>
					<div class="error">
						<?php if(isset($_SESSION['error']['College'])) echo $_SESSION['error']['College']; ?>
					</div>
				</div>
				<div class="input-field col m6 s12">
					<input required name="Mobile_Number" type="text"/>
					<label for="Mobile_Number">Mobile No.</label>
					<div class="error">
						<?php if(isset($_SESSION['error']['Mobile_Number'])) echo $_SESSION['error']['Mobile_Number']; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<button class="btn col m2 offset-m5 s8 offset-s2" type="submit" name="Sign_In" >Sign In</button>
			</div>
			</form> 
		<?php
			if($_SESSION['state'] == "-12" && !isset($_SESSION['error']['database']))
			{
				unset($_SESSION['error']); 
				$_SESSION['state'] ="11";
			}
		}
		else 
		{
			if(isset($_SESSION['error']['database'])) { ?>
			<div class="toast-content">
				<?php echo $_SESSION['error']['database']; ?>
			</div>
		<?php	}
			session_destroy();  
		}
		?>
		</div>
	</div>
</main>
</body>
</html>