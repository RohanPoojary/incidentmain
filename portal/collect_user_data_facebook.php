<!DOCTYPE html>
<html>
<head>
  <title>Collect Facebook</title>
</head>
<body>
<?php

session_start();

/******Improting Facebook API Files**************/
require_once 'includes/Facebook/autoload.php';
require_once 'includes/credentials.php';
$fb = new Facebook\Facebook([
  'app_id' => $appid,
  'app_secret' => $appsecret,
  'default_graph_version' => 'v2.2',
  ]);

$helper = $fb->getRedirectLoginHelper();

/******Getting Token From Facebook**************/
try {
  $accessToken = $helper->getAccessToken();
} catch(Exception $e) {
  // When Graph returns an error
  if($_SESSION['state'] == "30")
  {    
    $_SESSION['state'] = "-3";
    $_SESSION['failure3'] = "Process Failed! Try again.";
    header('location: index.php');
    exit;
  } 

  $_SESSION['state'] = "-11";
  header('location: index.php');
  
  exit;
} 

/******Storing Token In Sessions For Further Use**************/
if (isset($accessToken)) {

  $_SESSION['token'] = (string) $accessToken;

  $fb->setDefaultAccessToken($_SESSION['token']);
  /******Retrieving Users FB Profile With Display Picture**************/
  try {
    $response = $fb->get('/me?fields=email,name,gender,link');
   // $response = $fb->get('/me?access_token='.$_SESSION['token']);
    $userNode = $response->getGraphUser();
    $responseDp = $fb->get('/me/picture?redirect=false&type=small');
    $userNodeDp = $responseDp->getGraphUser();
  } catch(Facebook\Exceptions\FacebookResponseException $e) {
    if($_SESSION['state'] == "30")
    {    
      $_SESSION['state'] = "-3";
      $_SESSION['failure3'] = "Process Failed! Try again.";
      header('location: index.php');
      exit;
    } 

    $_SESSION['state'] = "-1";
    $_SESSION['failure1'] = "Process Failed! Try again.";
    header('location: index.php');
    exit;
    // When Graph returns an error
    //echo 'Graph returned an error: ' . $e->getMessage();
    //exit;
  } catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
    if($_SESSION['state'] == "30")
    {    
      $_SESSION['state'] = "-3";
      $_SESSION['failure3'] = "Process Failed! Try again.";
      header('location: index.php');
      exit;
    } 
    $_SESSION['state'] = "-1";
     $_SESSION['failure1'] = "Process Failed! Try again.";
    header('location: index.php');
    //echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
  }

  /**************Storing Data In Sessions******************/
  $_SESSION['id'] = $userNode->getProperty('id');
  $_SESSION['full_name']=$userNode->getName();
  $_SESSION['email']=$userNode->getProperty('email');
  $_SESSION['image_url'] = $userNodeDp->getProperty('url');
  

  if($_SESSION['state'] == "30")
  {
    $_SESSION['state'] = "31";
    header('location: get-id.php');
    exit;
  }
    
  $_SESSION['state'] = "11";
  // echo "Success";
  header('location: create-user.php#');
   exit;

}
else{

  if($_SESSION['state'] == "30")
  {    
    $_SESSION['state'] = "-3";
    $_SESSION['failure3'] = "Process Failed! Try again.";
    header('location: index.php');
    exit;
  } 
  $_SESSION['state'] = "-1";
   $_SESSION['failure1'] = "Process Failed! Try again.";
  header('location: index.php');
  exit;
}
?>
</body>
</html>
