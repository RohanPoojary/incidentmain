<?php session_start(); ?>
<?php

  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  if(!isset($_SESSION['state']))
  {
    $_SESSION['state'] = "30";
  }else if($_SESSION['state'][0]!="3")
  {
    $_SESSION['state'] = "30";
  }

  require_once ('includes/Google/autoload.php');
  require_once('includes/Facebook/autoload.php');
  require_once('includes/credentials.php');
  require_once("includes/db_connect.php");
  require_once("includes/functions.php");

  $client = new Google_Client();
  $client->setClientId($client_id);
  $client->setClientSecret($client_secret);
  $client->setRedirectUri($redirect_uri);

  $client->addScope("email");
  $client->addScope("profile");

  $service = new Google_Service_Oauth2($client);

  if(isset($_GET['logout']))
  {
    unset($_SESSION['access_token']);
  }
  $authUrl = $client->createAuthUrl();

  $fb = new Facebook\Facebook([
  'app_id' => $appid,
  'app_secret' => $appsecret,
  'default_graph_version' => 'v2.2',
  ]);

  $helper = $fb->getRedirectLoginHelper();
  $permissions = ['email'];
  $loginUrl = $helper->getLoginUrl($incommingurl, $permissions);  
?>
<!DOCTYPE html>
<html>
<head>
	<title>Incident Portal | Forgot ID</title>
	<?php include_once("../headers.php"); ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="../css/portal-index.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>

</head>
<body>
<?php include_once("../loader.php"); ?>
<?php include_once("../menu.php"); ?>
<main>
	<div class="container">
		<div class="img-wrapper">
			<img class="responsive-img" src="<?php echo $domain;?>/images/Logo.png">
		</div>
		<h4 class="center-align">Forgot ID</h4>
		<div class="form-wrapper">
			<div data-wrapper="Sign In" class="button-wrapper">
				<a rel="nofollow" class="waves-effect waves-light btn red" href="<?php echo $authUrl; ?>">Google</a>
				<a rel="nofollow" class="waves-effect waves-light btn indigo" href="<?php echo $loginUrl; ?>">Facebook</a>
			</div>
		</div>
	</div>
</main>
</body>
</html>