<?php session_start(); ?>
<?php
  require_once('includes/credentials.php');
  require_once("includes/db_connect.php");
  require_once("includes/functions.php");

  	if($_SESSION['state'] == "12")
    {
    	unset($_SESSION['state']);
    	$id = $_SESSION['account_id'];
      $email = $_SESSION['email'];
      $full_name = $_SESSION['full_name'];
      $college= $_SESSION['college'];
      $mobile_number = $_SESSION['mobile_number'];
      $image_url = $_SESSION['image_url'];
      $degree = $_SESSION['degree'];
      $year_of_study = $_SESSION['year_of_study'];
	}
	else if($_SESSION['state'] == "31")
    {
      unset($_SESSION['state']);
      $email = trim($_SESSION['email']);
      if(!(isset($email) && trim($email)!=""))
        $error = "No user with this Email ID";
      else if(strlen($email) > 35)
        $error = "No user with this Email ID";
      else if(!is_present('accounts','email',$email))
        $error = "No user with this Email ID";
      if(!isset($error))
      {
        $email = mysqli_real_escape_string($connection,$email);
        $query = "SELECT account_id,email,full_name,college,mobile_number,image_url,degree,year_of_study FROM accounts WHERE email='{$email}'";
        $result = mysqli_query($connection,$query);
        if($result && mysqli_num_rows($result)>0)
        {
          $row = mysqli_fetch_assoc($result);
          $id = $row['account_id'];
          $email = $row['email'];
          $full_name = $row['full_name'];
          $college= $row['college'];
          $mobile_number = $row['mobile_number'];
          $image_url = $row['image_url'];
          $degree = $row['degree'];
          $year_of_study = $row['year_of_study'];
        }
        else
        {
          $_SESSION['state'] = "-3";
          $_SESSION['failure3'] = "Process failed! Try again.";
          header('location: index.php');
          exit;
        }
      }
      else
      {
        $_SESSION['state'] = "-3";
        $_SESSION['failure3'] = $error;
        header('location: index.php');
        exit;
      } 
    }else {
    	header('location: index.php');
        exit;
    }
?>
  <!DOCTYPE html>
<html>
<head>
	<title>Incident Portal | Get ID</title>
  <meta name="robots" content="NOINDEX, NOFOLLOW"/>
	<?php include_once("../headers.php"); ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="../css/portal-index.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>

</head>
<body>
<?php include_once("../loader.php"); ?>
<?php include_once("../menu.php"); ?>
<main>
	<div class="container">
		<div class="row">
			<div class="col s12 m6 offset-m3">
				<div class="card large">
					<div class="card-image">
						<img src="<?php echo $image_url; ?>">
						<span class="card-title"><?php echo $full_name; ?></span>
					</div>
					<div class="card-content">
						<img class="bg" src="<?php echo $domain;?>/images/Logo-text.png" />
						<p class="card-id">ID: <?php echo $id; ?></p>
						<div class="row">
							<div class="col s12 m6"><label>Name:</label><?php echo $full_name; ?></div>
							<div class="col s12 m6"><label>College:</label><?php echo $college; ?></div>
						</div>
						<div class="row">
							<div class="col s12 m6"><label>Email:</label><?php echo $email; ?></div>
							<div class="col s12 m6"><label>Degree:</label>
              <?php 
              $number = $year_of_study;
              $sup = "th";
              if($number == 1)
                $sup = "st";
              else if ($number == 2)
                $sup = "nd";
              else if ($number == 3)
                $sup = "rd";
              echo $number.$sup." year-".$degree; 
              ?>
              </div>
						</div>
						<div class="row">
							<div class="col s12 m6"><label>Mobile No:</label><?php echo $mobile_number; ?></div>
						</div>
            <div class="notice red-text">Please make a note of your ID</div>
					</div>
				</div>
        <a class="btn" href="index.php">Go Back</a>
			</div>
		</div>
	</div>
</main>
</body>
</html>