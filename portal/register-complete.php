<?php 
  session_start(); 
  require_once("includes/db_connect.php");
  require_once("includes/functions.php"); 

  if(!isset($_POST["Submit"]))
  {
  	session_destroy();
  	header('location: index.php');
  }
   
  $required_fields = array();
  $max_lengths = array();
  $min_lengths = array();
  $event_id = $_POST["events"];
	$event = get_event($event_id);
  array_push($required_fields, "account_id");
  $max_lengths["account_id"] = '10';
  $min_lengths["account_id"] = '4';

  if(!isset($_POST['accomodation'])) {
    $_POST["accomodation"] = "off";
  }

  if($_POST['accomodation'] == "on")
  {
  	for($i=1;$i<=5;$i++)
    {
  	  $id = "Day".$i;
  	  $max_lengths[$id] = '3'; 
    } 
  }
  
  
  validate_presence($required_fields);

  validate_max_length($max_lengths);

  validate_min_length($min_lengths);
  
  $count = 0;
  if($_POST['accomodation'] == "on")
  {
  	for($i=1;$i<=5;$i++)
    {
      $id = "Day".$i;
      $value = trim($_POST[$id]);
      if(!(isset($value) && $value!="0" && $value!=""))
      	$count = $count + 1;
      else if(!is_numeric($value))
        $_SESSION['error'][$id] = "Input value has to be numeric";  
    }
    
    if($count == "5")
  	  $_SESSION['error']['acc'] = "All entries of accomodation cant be zero / blank";
  }else if(!isset($_POST['events'])) {
     $_SESSION['error']['events'] = "Participant has to either participate or avail accomodation";
  }

  if(isset($_POST['events'])) {
    $value = "participants";
    $ids = explode(",",trim($_POST[$value]));
    $ids = array_map('trim', $ids);
    // print_r($ids);
    // print_r($_POST);
    if(!in_array(trim($_POST['account_id']),$ids))
    {
      $_SESSION['error'][$value] = "Your Account Id must be present in participants Account Id";  
    }
    else 
    {
      foreach ($ids as $id) {
        if(!is_present('accounts','account_id',$id))
        {
          $_SESSION['error'][$value] = "Account Id {$id} doesnt exist";
          break;
        }
        else if(is_present('participants','id',$id,'event_id',$event_id))
        {
          $_SESSION['error'][$value] = "User with ID {$id} has already registered for this event";
          break;
        }
      }
    }
  }
  // print_r($_SESSION);
  if(empty($_SESSION['error']))
  {

    $cap = mysqli_real_escape_string($connection,$_POST['account_id']);
    $team = $_POST["team_name"];

    mysqli_autocommit($connection, FALSE);
    mysqli_begin_transaction($connection, MYSQLI_TRANS_START_READ_WRITE);

    if(isset($_POST["events"])) {
      $key = $event_id;
      $value = "participants";
      $ids = explode(",",trim($_POST[$value]));
      $ids = array_map('trim', $ids);
      $cap = trim($cap);
      $key = mysqli_real_escape_string($connection,$key);
      $team = mysqli_real_escape_string($connection, $team);
      foreach ($ids as $id) {
        $id = mysqli_real_escape_string($connection,$id);
        $query = "INSERT INTO participants VALUES('{$id}','{$key}','{$cap}', '{$team}', NULL)";
        // echo $query;
        $result = mysqli_query($connection,$query);
        if(!($result && mysqli_affected_rows($connection)>0))
        {
          // echo $query;
          mysqli_rollback($connection);
          $_SESSION['state'] = "-2";
        }else{
          mysqli_free_result($result);
        }
      }
    }
    // print_r($_POST);
    if($_POST['accomodation'] == "on")
    {
      for($i=1;$i<=5;$i++)
      {
        $id = "Day".$i;
        $value[$i] = trim($_POST[$id]);

        if(!(isset($value[$i]) && $value[$i]!=""))
          $value[$i] = "0";
      }

      $query = "INSERT INTO accomodation VALUES(NULL, '{$cap}','".$value['1']."','".$value['2']."','".$value['3']."','".$value['4']."','".$value['5']."')";
      $result = mysqli_query($connection,$query);
      if(!($result && mysqli_affected_rows($connection)>0))
      {
        echo $query;
        mysqli_rollback($connection);
        $_SESSION['state'] = "-2";
      }else{
        mysqli_free_result($result);
      }   
    }
    // echo($_SESSION["state"]. "<br/>");
    mysqli_commit($connection);

    mysqli_close($connection);
    
    if(isset($_SESSION['state']) && $_SESSION['state'] == "-2")
    {
      $_SESSION['failure2'] = " Registration failed! Please try again.";
      // header('location: index.php');
      exit;
    }
   else{
      $_SESSION['state'] = "22";
      header('location: register.php');
      exit;
    }
  }
  else
  {
  	$_SESSION['state'] = "-22";
    // echo print_r($_SESSION["error"]);
  	header('location: register.php');
    exit;
  }

?>

