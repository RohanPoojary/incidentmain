<?php 
  require_once("includes/db_connect.php");
  session_start(); 
  require_once("includes/functions.php"); 
  if(!(isset($_SESSION['full_name']) && isset($_SESSION['email'])) && !isset($_POST['Sign_In'])){
    $_SESSION['state'] = "10";
    
    header('location: index.php');
    exit;
  }  

  if(isset($_POST["Sign_In"]))
  {
  	multiple_entry('Email',$_SESSION['id']);
    
  	if(isset($_SESSION['multiple']))
  	{
      $_SESSION['failure1'] = $_SESSION['multiple'];
      unset($_SESSION['multiple']);
      $_SESSION['state'] = "-1";
      header('location: index.php');
      exit;
  	}
    else
    {
      $required_fields = array("Full_Name","Email","College","Mobile_Number","Degree","Year_Of_Study");

      validate_presence($required_fields);
 
      $max_lengths = array("Full_Name"=>30,"Email"=>35,"College"=>50,"Mobile_Number"=>13,"Degree"=>40,"Year_Of_Study"=>2);

      validate_max_length($max_lengths);
    
      $min_lengths = array("Full_Name"=>3,"Email"=>7,"College"=>2,"Mobile_Number"=>10,"Degree"=>2);
      
      validate_min_length($min_lengths);

      if(!is_numeric($_POST["Mobile_Number"]))
        $_SESSION['error']["Mobile_Number"] = "Invalid Mobile Number";
      
      if(!is_numeric($_POST["Year_Of_Study"]))
        $_SESSION['error']["Year_Of_Study"] = "Invalid year of study";

      if(empty($_SESSION['error']))
      {

        do{
          $account_id = generate_account_id($_POST["Full_Name"]);
          $query = "SELECT account_id FROM accounts WHERE account_id = '{$account_id}'";
          $result = mysqli_query($connection,$query);
          if(mysqli_num_rows($result) == "0")
            break;
          mysqli_free_result($result);
        }while(1);

        mysqli_free_result($result);
      
        $_SESSION['account_id'] = $account_id;
        $_SESSION['full_name'] = trim($_POST["Full_Name"]);
        $_SESSION['email'] = trim($_POST["Email"]);
        $_SESSION['college'] = trim($_POST["College"]);
        $_SESSION['mobile_number'] = trim($_POST["Mobile_Number"]);
        $_SESSION['degree'] = trim($_POST["Degree"]);
        $_SESSION['year_of_study'] = trim($_POST["Year_Of_Study"]);

        $account_id = mysqli_real_escape_string($connection,$account_id);
        $id = mysqli_real_escape_string($connection,$_SESSION["id"]);
        $full_name = mysqli_real_escape_string($connection,trim($_POST["Full_Name"]));
        $email = mysqli_real_escape_string($connection,trim($_POST["Email"]));
        $college = mysqli_real_escape_string($connection,trim($_POST["College"]));
        $mobile_number = mysqli_real_escape_string($connection,trim($_POST["Mobile_Number"]));
        $image_url = mysqli_real_escape_string($connection,$_SESSION["image_url"]);
        $degree = mysqli_real_escape_string($connection,trim($_POST['Degree']));
        $year_of_study = mysqli_real_escape_string($connection,trim($_POST['Year_Of_Study']));

        $query = "INSERT INTO accounts VALUES('{$account_id}','{$id}','{$full_name}','{$email}','{$college}','{$mobile_number}','{$image_url}','{$degree}','{$year_of_study}')";
        $result = mysqli_query($connection,$query);
        if($result && mysqli_affected_rows($connection)>0)
        {
          mysqli_free_result($result);
          mysqli_close($connection);
          $_SESSION['state'] = "12";
          $message = "
          <html>
          <title>Registration Successful</title>
          <body>
          <h2>Registration Successful</h2>
          <p>Here is your Inci ID: <b>$account_id</b></p>
          <p>To know more please visit our website <a href='http://incident.co.in'>http://incident.co.in</a></p>
          <br/>
          <footer>
          Regards,<br/>
          Team Incident <br/>
          <a href='mailto: incidentpublicity@nitk.edu.in'>incidentpublicity@nitk.edu.in</a><br/>
          NITK Surathkal<br/>
          </footer>
          </body>
          </html>";
          $headers[] = 'MIME-Version: 1.0';
          $headers[] = 'Content-type: text/html; charset=iso-8859-1';
          $headers[] = "From: Team Incident <incidentpublicity@nitk.edu.in>";
          mail($email, "Registration Successful", $message, implode("\r\n", $headers));

          header('location: get-id.php');
          exit;
        }
        else
        {
          mysqli_close($connection);
          $_SESSION['state'] = "-12";
          $_SESSION['error']['database'] = "Database error try again!";
          header('location: create-user.php');
          exit;
        }

      }
      else
      {
        mysqli_close($connection);
        $_SESSION['state'] = "-12";
        header('location: create-user.php');
        exit;
      }
    }
  }
  else
  {
    mysqli_close($connection);
    $_SESSION['state']="10";
    header('location: index.php');
    exit;
  }
?>