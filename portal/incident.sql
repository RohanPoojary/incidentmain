-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 13, 2017 at 01:56 PM
-- Server version: 5.6.32-78.1-log
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `enginee8_inci`
--

-- --------------------------------------------------------

--
-- Table structure for table `accomodation`
--

CREATE TABLE IF NOT EXISTS `accomodation` (
  `key_id` int(11) NOT NULL,
  `id` varchar(10) NOT NULL,
  `day1` int(11) DEFAULT NULL,
  `day2` int(11) DEFAULT NULL,
  `day3` int(11) DEFAULT NULL,
  `day4` int(11) DEFAULT NULL,
  `day5` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accomodation`
--

INSERT INTO `accomodation` (`key_id`, `id`, `day1`, `day2`, `day3`, `day4`, `day5`) VALUES
(2, 'Jai9806', 0, 0, 0, 0, 0),
(3, 'HAR7309', 1, 1, 1, 1, 0),
(4, 'TOI4775', 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `account_id` varchar(10) NOT NULL,
  `id` varchar(50) NOT NULL,
  `full_name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `college` varchar(60) NOT NULL,
  `mobile_number` varchar(15) NOT NULL,
  `image_url` varchar(220) NOT NULL,
  `degree` varchar(50) NOT NULL,
  `year_of_study` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`account_id`, `id`, `full_name`, `email`, `college`, `mobile_number`, `image_url`, `degree`, `year_of_study`) VALUES
('AKH1492', '1237783172970307', 'Akhil Kumar D', 'akhilkmr171@hotmail.com', 'NITK', '8748816552', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/15698296_1222244954524129_5609284195610454577_n.jpg?oh=61dedcfdfac7473e2d609971183d7ff6&oe=59210995', 'B Tech', 1),
('AKS3101', '114965620000541445219', 'Akshay Kungulwar', 'akshaykungulwar@gmail.com', 'NITK SURATHKAL', '9980876797', 'https://lh5.googleusercontent.com/-oRts4N3_pLA/AAAAAAAAAAI/AAAAAAAAG0M/orUcTmyLQ8o/photo.jpg', 'B.Tech', 4),
('ASW8799', '937897753014474', 'Aswanth Parakkettu', 'aswanthbhavan@gmail.com', 'NITK Surathkal', '9562978698', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/14610928_884776731659910_8071662232856798247_n.jpg?oh=3c88d95580c3491eaa6ec6714ab9b537&oe=58E2DC2B', 'B tech', 1),
('AYA8097', '102756690660832989149', 'Ayam Jain', 'ayam.jain@gmail.com', 'NITK', '9741398314', 'https://lh5.googleusercontent.com/-D0sNOup1ih0/AAAAAAAAAAI/AAAAAAAAASo/XZDbWfJORn8/photo.jpg', 'B.Tech.', 2),
('CHA4210', '113161256328622524732', 'Chandan Chowdary', 'chandanchowdary009@gmail.com', 'Reva University', '9845399081', 'https://lh6.googleusercontent.com/-Y9kw9djGggg/AAAAAAAAAAI/AAAAAAAADfE/Gg7YPC8oX8U/photo.jpg', 'B tech engineering', 1),
('DAK2568', '113229064362075171828', 'Dakshath KM', 'dakshu1651998@gmail.com', 'SJCE', '9449920002', 'https://lh4.googleusercontent.com/-KgQKASSA5-8/AAAAAAAAAAI/AAAAAAAAAHI/d8tz5IawUIE/photo.jpg', 'B.E.', 1),
('GAG7076', '110007835918138281803', 'gagan ramesh', 'gaganramesh27@gmail.com', 'SJCE', '8892651681', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', 'Engineering', 1),
('GAN8764', '107651825942604470610', 'Ganesh P Nischay', 'ganeshnischaypgpn@gmail.com', 'NITK', '8050435989', 'https://lh5.googleusercontent.com/-BqINBava5gY/AAAAAAAAAAI/AAAAAAAAAQY/CIKYiv_pHs8/photo.jpg', 'B.TECH', 1),
('GUR4781', '116254321972674798005', 'Gurupungav Narayanan', 'gurupungavn@gmail.com', 'NITK', '9008791776', 'https://lh4.googleusercontent.com/-wMUpgDSF0Ko/AAAAAAAAAAI/AAAAAAAADgA/hfjFWKkXWCk/photo.jpg', 'B.TECH', 1),
('HAR7309', '102539281428208738653', 'Hari Shankar S', 'harishank8@gmail.com', 'NITK', '8147766991', 'https://lh3.googleusercontent.com/-WiO9d-mr630/AAAAAAAAAAI/AAAAAAAAAR4/ymvkYK6Iwvk/photo.jpg', 'B.Tech', 4),
('JAI9806', '101294900353575577599', 'Jai Deep', 'jaideepm83@gmail.com', 'Cti mysore', '9972222223', 'https://lh4.googleusercontent.com/-L9oL86LcsQI/AAAAAAAAAAI/AAAAAAAAAEg/7fqj4g8kFVk/photo.jpg', 'Diploma', 5),
('KAR9744', '10202449148388223', 'Kartikeya Srivastava', 'kartikdil@gmail.com', 'NITK', '9971388341', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/14433202_10201986265856449_3751351035357765026_n.jpg?oh=51754aa1ee1e9b4dea905ae742c6a87b&oe=58E6A914', 'MBA', 1),
('KIS3698', '1838916219655362', 'Kishan Jagadeesh', 'kishankuthaje@gmail.com', 'National Institution of Technology  Karnataka', '9447734133', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/12033142_1722521077961544_2422312298792030690_n.jpg?oh=3a76d8d02bd4c72e2300ef0c89ff5241&oe=591D2809', 'Btech', 1),
('KSH2614', '1458208517531341', 'Kshitij Bhandari', 'kshitij.sporty@yahoo.co.in', 'NITK', '9930411090', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/15941020_1457493960936130_7716977294917387817_n.jpg?oh=4a377c9142c67bf696c3b93f35202d14&oe=5918E0EC', 'Btech', 1),
('MAN3441', '114511058365080627753', 'Manish Chandra', 'chandramanish427@gmail.com', 'NITK', '9845817435', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', 'Btech', 1),
('MAN8104', '115664873160906456379', 'Manmohan Totala', 'manmohant97@gmail.com', 'RVCE', '8722960001', 'https://lh6.googleusercontent.com/-9e9doaogB4Q/AAAAAAAAAAI/AAAAAAAAAIA/O_zd88ACygU/photo.jpg', 'B.E', 2),
('MAN9969', '115953614457571046588', 'manvendra singh', 'gaura00007@gmail.com', 'NITK SURATHKAL', '8277572363', 'https://lh5.googleusercontent.com/-qPo3RnmJotw/AAAAAAAAAAI/AAAAAAAAAFI/UKf4rersIF8/photo.jpg', 'M.TECH', 1),
('MAY2921', '765213520299960', 'Mayank Tripathi', 'mayanktripathi045@gmail.com', 'NITK', '7697416733', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/13882188_674887302665916_3722228093945923426_n.jpg?oh=8afaf30d6cc5e1725196227da7aea085&oe=58E08038', 'MCA', 1),
('MUK8149', '110544005548774240448', 'mukul nagar', 'mukulnagar1995@gmail.com', 'Nitk', '9980862965', 'https://lh3.googleusercontent.com/-i6mbQrKgNF0/AAAAAAAAAAI/AAAAAAAAB1U/_GKll-bOrAc/photo.jpg', 'Btech', 4),
('NIH4459', '1345138378892245', 'Nihar Chitnis', 'niharchitnis@gmail.com', 'National Institite of Technology, Karnataka', '8951791633', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/15941069_1344558518950231_5411485721234382296_n.jpg?oh=563afc566ccacc8ae2249e9814550b77&oe=5918D2DB', 'BTech', 1),
('PAV8098', '116805557172983186806', 'Pavan Gowda', 'pavangwd97@gmail.com', 'Sjce', '7411447397', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', 'BE', 1),
('PRA2472', '1289662084441711', 'Prajval Murali', '26prajval98@gmail.com', 'NITK', '9449642887', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/11034045_780541352020456_1250147708473774607_n.jpg?oh=6a2e50f787bf7793ed487433347de883&oe=590DD8EF', 'B tech', 1),
('PRE5109', '115082823149721421062', 'Preetham Upadhya', 'upadhyapreetham@gmail.com', 'PES University', '7259708234', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', 'B.Tech', 1),
('SHE2731', '1440141496006620', 'Sheetal Pasam', 'sheetalpasam@yahoo.co.in', 'nitk', '8050224245', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/15873588_1439366846084085_1037482035360911988_n.jpg?oh=14ce18c5befe7d14288fd2087cdbc31e&oe=58E03FE0', 'b.tech', 1),
('SHR8615', '113736646086405787398', 'Shriganesh Neeramule', 'snganeshn19@gmail.com', 'NITK', '7760469622', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', 'BTech ECE', 2),
('SRI3857', '104802697545238301275', 'sriram sharath', 'gsriramsharath@gmail.com', 'nitk', '7996803369', 'https://lh4.googleusercontent.com/-MsPQ56BjjtU/AAAAAAAAAAI/AAAAAAAAABQ/FibGzYVQkuE/photo.jpg', 'b.tech', 1),
('SWA5200', '100729759217281655451', 'Swastik Udupa', 'swastikudupa@gmail.com', 'NITK', '9740641023', 'https://lh6.googleusercontent.com/-xF3PBn-zOhc/AAAAAAAAAAI/AAAAAAAAABk/U5i8cvdHph8/photo.jpg', 'B.Tech', 2),
('TEE4268', '1429744233704878', 'Teena Johnson', 'teenaj2007@gmail.com', 'Nitk surathkal', '9480166062', 'https://scontent.xx.fbcdn.net/v/t1.0-1/s50x50/15780902_1422001217812513_7150592823847673140_n.jpg?oh=b4a51e7f17bd199d60b7bbe5aba6e3e9&oe=591E65B9', 'M.tech', 1),
('TOI4775', '110118202991574133013', 'Toini Alweendo', 'toinialweendo@gmail.com', 'Acharya Institute Of Graduate Studies', '8197199841', 'https://lh6.googleusercontent.com/-GKA2HXbdpHE/AAAAAAAAAAI/AAAAAAAAB0c/iT75FVxob6c/photo.jpg', 'Bachelor Of Computer Application', 2),
('UDA6397', '103487953017823157773', 'Uday Kumar', 'uday.adroit1@gmail.com', 'NITK', '9505070175', 'https://lh6.googleusercontent.com/-Ftfq3tZr_W4/AAAAAAAAAAI/AAAAAAAAAA8/PFpCvUxYTC4/photo.jpg', 'Btech', 1);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `category` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `category`) VALUES
(0, 'DJ War', 'Beach Events'),
(1, 'Promenade', 'Dance'),
(2, 'Step Up - Solo', 'Dance'),
(3, 'Tandav', 'Dance'),
(4, 'Step Up - Duet', 'Dance'),
(5, 'Raagalaya', 'Music'),
(6, 'Center Stage', 'Music'),
(7, 'Unplugged', 'Music'),
(8, 'Dhvanik', 'Music'),
(9, 'Pulse', 'Music'),
(10, 'Bandish', 'Music'),
(14, 'Haute Couture', 'Fashion'),
(15, 'Mock Stock', 'Biz Events'),
(16, 'Lobbying', 'Biz Events'),
(17, 'Biz Quiz', 'Biz Events'),
(18, 'Jam', 'Lit Events'),
(19, 'Lone Wolf Quiz', 'Lit Events');

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE IF NOT EXISTS `participants` (
  `id` varchar(10) NOT NULL,
  `event_id` int(11) NOT NULL,
  `cap_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`id`, `event_id`, `cap_id`) VALUES
('TOI4775', 2, 'TOI4775');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accomodation`
--
ALTER TABLE `accomodation`
  ADD PRIMARY KEY (`key_id`), ADD KEY `accomodation_ibfk_1` (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`,`event_id`), ADD KEY `cap_id` (`cap_id`), ADD KEY `event_id` (`event_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accomodation`
--
ALTER TABLE `accomodation`
  MODIFY `key_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `accomodation`
--
ALTER TABLE `accomodation`
ADD CONSTRAINT `accomodation_ibfk_1` FOREIGN KEY (`id`) REFERENCES `accounts` (`account_id`);

--
-- Constraints for table `participants`
--
ALTER TABLE `participants`
ADD CONSTRAINT `participants_ibfk_1` FOREIGN KEY (`id`) REFERENCES `accounts` (`account_id`),
ADD CONSTRAINT `participants_ibfk_2` FOREIGN KEY (`cap_id`) REFERENCES `accounts` (`account_id`),
ADD CONSTRAINT `participants_ibfk_3` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
