<?php
  
  function multiple_entry($field,$id)
  {
    global $connection,$error;
    $value1 = mysqli_real_escape_string($connection,trim($_POST[$field]));
    $id = mysqli_real_escape_string($connection,$id);
    $query="SELECT id,email FROM accounts WHERE email='{$value1}' OR id='{$id}'";
    $result = mysqli_query($connection,$query);
    if(mysqli_num_rows($result)>0)
    {
      $_SESSION['multiple']="User already exists!";
    }
  }

  function fieldname_as_text($fieldname)
  {
	  $fieldname = str_replace("_"," ",$fieldname);
	  $fieldname = ucfirst($fieldname);
	  return $fieldname;
  }

  function has_presence($var)
  {
    if(isset($var) && $var!="")
      return 1;
    return 0;
  }
   
  function validate_presence($require_fields)
  {  
    foreach($require_fields as $field)
	  {
	    $value = trim($_POST[$field]);
	    if(!has_presence($value))
	    {
		    $_SESSION['error'][$field]=fieldname_as_text($field)." cant be blank";
        //echo $_SESSION['error'][$field];
	    }
    }	
  }

  function has_max_length($field,$max)
   {
	   $value = trim($_POST[$field]);
	   if(strlen($value)>$max)
	   {
		   return 1;
	   }
	   else 
		   return 0;
   }

   function validate_max_length($require_fields)
   {
	   global $error;
	   foreach($require_fields as $field=>$max)
	   {
		   if(has_max_length($field,$max) && !isset($_SESSION['error'][$field]))
		   {
				   $_SESSION['error'][$field]=fieldname_as_text($field)." cant exceed ".$max." characters";
          // echo $_SESSION['error'][$field];
		   }
	   }
   }
   
   function has_min_length($field,$min)
   {
	   $value = trim($_POST[$field]);
	   if(strlen($value)<$min)
	   {
		   return 1;
	   }
	   else 
		   return 0;
   }

   function validate_min_length($require_fields)
   {
	   global $error;
	   foreach($require_fields as $field=>$min)
	   {
		   if(has_min_length($field,$min) && !isset($_SESSION['error'][$field]))
		   {
				   $_SESSION['error'][$field]=fieldname_as_text($field)." cant be less than ".$min." characters";
           //echo $_SESSION['error'][$field];
		   }
	   }
   }  
 
   function generate_account_id($full_name)
   {
      $full_name= trim($full_name);
      $account_id = substr($full_name, 0,3).mt_rand(1000,9999);
      $account_id = str_replace(" ","_",$account_id);
      return $account_id;
   }

   function is_present()
   {
     global $connection;
  
     $numargs = func_num_args();
     $table = func_get_arg(0);
     $query = "SELECT * FROM {$table} WHERE ";

     for($i=1;$i+1<$numargs;$i=$i+2)
     {
       $field = mysqli_real_escape_string($connection,func_get_arg($i));
       $value = mysqli_real_escape_string($connection,func_get_arg($i+1));
       $query.="{$field} = '{$value}'";
       if($i+2 != $numargs)
        $query.=" AND ";
     }

     $result = mysqli_query($connection,$query);
     if($result && mysqli_num_rows($result)>0)
      return 1;
     else
      return 0;
   }

   function get_event($event_id)
   {
     global $connection;

     $event_id = mysqli_real_escape_string($connection,trim($event_id));
     $query = "SELECT name FROM events WHERE  id='{$event_id}'";
     $result = mysqli_query($connection,$query);
     if($result && mysqli_num_rows($result)>0)
     {
       $row = mysqli_fetch_assoc($result);
       return $row['name'];
     }
   }
?>