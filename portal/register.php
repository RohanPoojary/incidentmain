<?php session_start(); ?>
<?php
  header("Location:index.php");

  require_once("includes/db_connect.php");
  require_once("includes/functions.php");

  $categories = array();

  $query = "SELECT id, name, category FROM events";
  $result = mysqli_query($connection, $query);
  if ($result && mysqli_num_rows($result) > 0) {
    while($row=mysqli_fetch_assoc($result)) {
      $category = $row["category"];
      $id = $row["id"];
      $name = $row["name"];
      $pair = array($id, $name);
      if(!array_key_exists($category, $categories)) 
        $categories[$category] = array();

      array_push($categories[$category], $pair);
    }
  }
?>
<!DOCTYPE html>
<html>
<head>
  <title>Incident Portal | Event Registration</title>
  <?php include_once("../headers.php"); ?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="../css/portal-index.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
  <script src="../js/portal.js"></script>
</head>
<body>
<?php include_once("../loader.php"); ?>
<?php include_once("../menu.php"); ?>
<main>
  <div class="container">
    <div class="img-wrapper small-img">
      <img class="responsive-img" src="<?php echo $domain;?>/images/Logo.png">
    </div>
    <h4 class="center-align">Event Registration</h4>
    <div class="form-wrapper form-large no-padding">
      <form method="POST" action="register-complete.php" onsubmit="return validateRegister()">
      <div class="row">
        <div class="input-field col m8 s12 offset-m2">
         <select id="events" name="events">
          <option value="" disabled selected>Select Event</option>
          <?php foreach ($categories as $category => $events) { ?>
          <optgroup label="<?php echo $category; ?>">
            <?php foreach ($events as $event) { ?>
                <option value="<?php echo $event[0]; ?>"><?php echo $event[1]; ?></option>
            <?php } ?>
          </optgroup>
          <?php } ?>
           </select>
          <label for="events">Event</label>
          <div class="error"><?php if(isset($_SESSION['error']['events'])) echo $_SESSION['error']['events']; ?></div>
        </div>
      </div>
      <div class="row" id="team_name_row">
        <div class="input-field col m8 s12 offset-m2">
          <input type="text" name="team_name" id="team_name"/>
          <label for="team_name">Team Name</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col m8 s12 offset-m2">
          <input name="account_id" id="account_id" type="hidden">
        	<input id="participants"  type="hidden" name="participants">
          <div class="participants-ids">
          	<label>Enter Participant(s) ID</label>
          	<div class="participant row">
          		<input type="text" class="col s8" />
          		<button class="btn col s4" >Add</button>
          	</div>
          </div>
          <div class="error"><?php if(isset($_SESSION['error']["participants"])) echo $_SESSION['error']["participants"]; ?></div>
        </div>
      </div>
      <div class="row">
        <div class="input-field col m4 s12 offset-m4">
          <input type="checkbox" name="accomodation" id="accomodation"/>
          <label for="accomodation">Accommodation</label>
          <div class="error"><?php if(isset($_SESSION['error']['acc'])) echo $_SESSION['error']['acc']; ?></div>
        </div>
      </div>
       <div class="row" id="no-of-accomodation">
       <label class="col s12 m2">No of Accomodation</label>
        <div class="input-field col s4 m2">
          <input value="0" min="0" name="Day1" type="number">
          <label for="Day1">Day1</label>
          <div class="error"><?php if(isset($_SESSION['error']['Day1'])) echo $_SESSION['error']['Day1']; ?></div>
        </div>
        <div class="input-field col s4 m2">
          <input value="0" min="0" name="Day2" type="number">
          <label for="Day2">Day2</label>
          <div class="error"><?php if(isset($_SESSION['error']['Day2'])) echo $_SESSION['error']['Day2']; ?></div>
        </div>
        <div class="input-field col s4 m2">
          <input value="0" min="0" name="Day3" type="number">
          <label for="Day3">Day3</label>
          <div class="error"><?php if(isset($_SESSION['error']['Day3'])) echo $_SESSION['error']['Day3']; ?></div>
        </div>
        <div class="input-field col s4 m2">
          <input value="0" min="0" name="Day4" type="number">
          <label for="Day4">Day4</label>
          <div class="error"><?php if(isset($_SESSION['error']['Day4'])) echo $_SESSION['error']['Day4']; ?></div>
        </div>
        <div class="input-field col s4 m2">
          <input value="0" min="0" name="Day5" type="number">
          <label for="Day5">Day5</label>
          <div class="error"><?php if(isset($_SESSION['error']['Day5'])) echo $_SESSION['error']['Day5']; ?></div>
        </div>
      </div>
      <div class="row">
        <button class="btn col m2 offset-m5 s8 offset-s2" type="submit" name="Submit" >Register</button>
      </div>
      <div class="row">
        <a href="index.php" class="btn deep-orange col m4 offset-m2 s5 offset-s1">Dont have ID?</a>
        <a href="need-help.php" class="btn yellow darken-2 col m4 s5">Need Help?</a>
      </div>
      </form> 
    </div>
    <?php
     if(isset($_SESSION['state']) && $_SESSION['state'] == "-22")
      { 
      ?>

    <?php
        unset($_SESSION['error']);
        unset($_SESSION['state']);
      }
      if(isset($_SESSION['state']) && $_SESSION['state'] == "22")
      {
        session_destroy();
    ?>
        <div class="toast-content">Registration Successful</div>
    <?php 
      }
    ?>
  </div>
</main>
</body>
</html>