<?php 
  session_start();

  if(!isset($_SESSION['state']))
  {
   	$_SESSION['state'] = "10";
  }

  require_once ('includes/Google/autoload.php');
  require_once('includes/Facebook/autoload.php');
  require_once('includes/credentials.php');

  $client = new Google_Client();
  $client->setClientId($client_id);
  $client->setClientSecret($client_secret);
  $client->setRedirectUri($redirect_uri);

  $client->addScope("email");
  $client->addScope("profile");

  $service = new Google_Service_Oauth2($client);

  if(isset($_GET['logout']))
  {
    unset($_SESSION['access_token']);
  }
  $authUrl = $client->createAuthUrl();

  $fb = new Facebook\Facebook([
  'app_id' => $appid,
  'app_secret' => $appsecret,
  'default_graph_version' => 'v2.2',
  ]);

  $helper = $fb->getRedirectLoginHelper();
  $permissions = ['email'];
  $loginUrl = $helper->getLoginUrl($incommingurl, $permissions);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Portal | Incident - NITK Surathkal</title>
	<?php include_once("../headers.php"); ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="../css/01-portal-index.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
	<script src="../js/05-portal.min.js"></script>
</head>
<body>
<?php include_once("../loader.php"); ?>
<?php include_once("../menu.php"); ?>
<main>
	<div class="container">
		<div class="img-wrapper">
			<img class="responsive-img" src="<?php echo $domain;?>/images/Logo.png">
		</div>
		<h4 class="center-align">Incident Portal</h4>
		<p class="red-text center"><b>Registrations are CLOSED</sup></b></p>
		<!-- <div class="form-wrapper">
			<div data-wrapper="Create Account" class="button-wrapper">
				<a rel="nofollow" class="waves-effect waves-light btn red" href="<?php echo $authUrl; ?>">Google</a>
				<a rel="nofollow" class="waves-effect waves-light btn indigo" href="<?php echo $loginUrl; ?>">Facebook</a>
			</div>
			<div data-wrapper="Events Registration" class="button-wrapper">
				<a rel="nofollow" class="waves-effect waves-light btn teal" href="register.php">Register</a>
			</div>
			<br/>
			<a class="side-link float-left" href="need-help.php">Need Help?</a>
			<a rel="nofollow" class="side-link" href="forgot-id.php">Forgot ID?</a>
			<div class="nestaway-widget">
				<br/><br/>
				<a class="nestaway-btn btn orange waves-effect waves-light" href="#nestaway-widget">Nestaway-Long Term Solution</a>
				<div id="nestaway-widget" class="widget row">
					<div class="col s8 offset-s2">
						<a rel="nofollow" target="_blank" class="btn deep-orange waves-effect waves-light" href="http://nestaway.com">Visit Nestaway</a>
					</div>
					<img class="img responsive-img" src="../images/sponsors/Nestaway-Widget.jpg"/>
				</div>
			</div>
		</div> -->
		<?php
			$error = "";
			if(isset($_SESSION['state']) && ($_SESSION['state'] == "-1" || $_SESSION['state'] == "-11")) {
				if(isset($_SESSION['failure1'])) {
					$error = htmlentities($_SESSION['failure1']);
				}
				session_destroy();
			} else if(isset($_SESSION['state']) && $_SESSION['state'] == "-2") {
				if(isset($_SESSION['failure2'])) {
					$error = htmlentities($_SESSION['failure2']);
				}
				session_destroy();
			} else if(isset($_SESSION['state']) && $_SESSION['state'] == "-3") {
				if(isset($_SESSION['failure3'])) {
					$error = htmlentities($_SESSION['failure3']);
				}
				session_destroy();
			}
		?>
		<?php
			if($error) { ?>
			<div class="toast-content">
				<?php echo $error; ?>
			</div>
		<?php	}
		?>
	</div>
</main>
</body>
</html>