<?php

session_start();
require_once ('includes/Google/autoload.php');
require_once('includes/credentials.php');

$client = new Google_Client();
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);

$client->addScope("email");
$client->addScope("profile");
$service = new Google_Service_Oauth2($client);

if(isset($_GET['code']))
{
  try{
    $client->authenticate($_GET['code']);
    $_SESSION['access_token'] = $client->getAccessToken();
  }catch(Exception $e){
    if($_SESSION['state'] == "30")
    {
      $_SESSION['state'] = "-3";
      $_SESSION['failure3'] = "Process Failed! Try again.";
      header('location: index.php');
      exit;
    }
    $_SESSION['state'] = "-11";
    header('location: index.php');
    exit;
  }
  
}else{
  if($_SESSION['state'] == "30")
    {
      $_SESSION['state'] = "-3";
      $_SESSION['failure3'] = "Process Failed! Try again.";
      header('location: index.php');
      exit;
    }
    $_SESSION['state'] = "-11";
    header('location: index.php');
    exit;
}

/*if(isset($_GET['logout']))
{
  unset($_SESSION['access_token']);
  session_destroy();
}*/

if(isset($_SESSION['access_token']) && $_SESSION['access_token'])
{
  $client->setAccessToken($_SESSION['access_token']);
  $user = $service->userinfo->get();
  if(!empty($user->email)){
        $_SESSION['id'] = $user->id;
        $_SESSION['full_name'] = $user->name;
        $_SESSION['email'] = $user->email;
        $_SESSION['image_url'] = $user->picture;

        if($_SESSION['state'] == "30")
        {
          $_SESSION['state'] = "31";
          header('location: get-id.php');
          exit;
        }

        $_SESSION['state'] = "11";
        header('location: create-user.php');
  }
  else{
    if($_SESSION['state'] == "30")
    {
      $_SESSION['state'] = "-3";
      $_SESSION['failure3'] = "Process Failed! Try again.";
      header('location: index.php');
      exit;
    }
    $_SESSION['state'] = "-11";
    header('location: index.php');
    exit;
  }
}
else{

  if($_SESSION['state'] == "30")
  {
    $_SESSION['state'] = "-3";
    $_SESSION['failure3'] = "Process Failed! Try again."; 
    header('location: index.php');
    exit;
  }
    
  $_SESSION['state'] = "-11";
  header('location: index.php');
  exit;
}

?>
