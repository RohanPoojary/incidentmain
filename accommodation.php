<!DOCTYPE html>
<html>
<head>
	<title>Accommodation | Incident - NITK Surathkal</title>
	<meta name="description" content="Incident 2017 provides you good accomodation at reasonable costs here in NITK, Surathkal. Register for Incident before trying to book accomodation."/>
	<?php include_once("headers.php") ?>
	<link rel="stylesheet" type="text/css" href="css/accommodation.min.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>
<body>
<?php include_once("loader.php"); ?>
<?php include_once("menu.php"); ?>
<main>
<h1>Accommodation</h1>
<ul class="description">
	<li><b>Accommodation: Rs.150/- per day</b>. Food not included.</li>

	<li>Students have to pre-register <a href="<?php echo $domain;?>/portal/register.php">here</a> to avail accomodation facility.</li>

	<li>The payment has to be done at the registration desk inside main building.</li>

	<li>Students can avail accommodation facilities from 1st March 5:00 pm to 5th March 12:00 pm.</li>

	<li>Students should vacate the rooms by 6th March 8:00 am.</li>
</ul>
<div class="address">
	<div class="maps-wrapper">
		<iframe
	  frameborder="0" style="border:0"
	  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCIEaImM7u_-sD8trVq3q-684Pu72AzOlI
	    &q=National+Institute+of+Technology+Karnataka&zoom=15" allowfullscreen>
		</iframe>
	</div>
	<div class="address-details">
		<div class="sub-div">
			<h2>Location</h2>
			<p>NITK lies on the coastal belt of Karnataka, on the National Highway 66, at about 22 kms from Mangalore towards Udupi. 
			It lies close to Mangalore International Airport, Mangalore Railway Station and the Mangalore Port. Bus Routes from Mumbai, 
			Goa and Bangalore ( that terminate at Udupi-Manipal) pass through the College and visitors can disembark right away.</p>
		</div>
		<div class="sub-div">
			<h2>Contact</h2>
			<p>Anil Kumar N<br/><a href="anilkumar15.1995@gmail.com">anilkumar15.1995@gmail.com</a><br/><a href="tel:+919481364948">+919481364948</a></p>
		</div>
	</div>
</div>
</main>
</body>
</html>