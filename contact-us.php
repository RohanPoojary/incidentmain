<!DOCTYPE html>
<html>
<head>
	<title>Contact Us | Incident - NITK Surathkal</title>
	<meta name="description" content="If you have any query or qualms, feel free to contact Incident Team."/>
	<?php include_once("headers.php") ?>
	<link rel="stylesheet" type="text/css" href="css/contact.min.css">
	<script src="js/contact.min.js"></script>
</head>
<body>
<?php include_once("loader.php"); ?>
<?php include_once("menu.php"); ?>
<main>
<h1>Contact Us</h1>
<div class="contacts">
	<div class="center">
		<div class="contact">
			<h2>Sharath Kamath Shevgur</h2>
			<h4>Convenor</h4>
			<div class="icons">
				<a href="https://www.facebook.com/skshevgur" target="_blank" class="icon">
					<div class="icon-bg fb-bg"></div>
					<div class="icon-img fb-img"></div>
				</a>
				<a href="tel:+919591978298" target="_blank" class="icon">
					<div class="icon-bg phone-bg"></div>
					<div class="icon-img phone-img"></div>
				</a>
				<a href="mailto:skshevgur@gmail.com" target="_blank" class="icon">
					<div class="icon-bg"></div>
					<div class="icon-img"></div>
				</a>
			</div>
		</div>
	</div>
	<div class="contact">
		<h2>Kaushik R</h2>
		<h4>Marketing Convenor</h4>
		<div class="icons">
			<a href="https://www.facebook.com/kaushik.udupi" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+918105993035" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:incidentmarketing@nitk.edu.in" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Akshay Gurudath</h2>
		<h4>Marketing Coordinator</h4>
		<div class="icons">
			<a href="https://www.facebook.com/akshay.gurudath" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+918762176159" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:incidentmarketing@nitk.edu.in" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Ajay Rao</h2>
		<h4>Events Coordinator</h4>
		<div class="icons">
			<a href="https://www.facebook.com/AjayRao666" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+918123321833" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:ajayrao015@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>J Mahalakshmi Naidu</h2>
		<h4>Events Coordinator</h4>
		<div class="icons">
			<a href="https://www.facebook.com/maha.lakshminaidu.1291" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+91991677120" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:jmlnaidu@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Prajwal K</h2>
		<h4>Publicity Coordinator</h4>
		<div class="icons">
			<a href="https://www.facebook.com/prajwalk.poojary" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919449244208" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:prajwalk3795@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
		<div class="contact">
		<h2>Samanvith A</h2>
		<h4>Publicity Coordinator</h4>
		<div class="icons">
			<a href="https://www.facebook.com/samanvith.sam.9" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+917259461454" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:samanvith16@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Sandeep Sharma</h2>
		<h4>Chief Coordinator</h4>
		<div class="icons">
			<a href="https://www.facebook.com/sandeepsharma09" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919535086968" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:sharma.sandeepch@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>G Sri Raghav</h2>
		<h4>Chief Coordinator</h4>
		<div class="icons">
			<a href="https://www.facebook.com/raghav.chinna.1" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919980874218" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:raghav.chinna143@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>S Harshavardhana Reddy</h2>
		<h4>Chief Coordinator</h4>
		<div class="icons">
			<a href="https://www.facebook.com/harshavardhan.reddy.5" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919916168130" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:harsha12696@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>George C M</h2>
		<h4>Chief Coordinator</h4>
		<div class="icons">
			<a href="https://www.facebook.com/profile.php?id=100006329881383" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919916767393" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:georgecm95@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Ashfaq Anwar</h2>
		<h4>Proshows Coordinator</h4>
		<div class="icons">
			<a href="https://www.facebook.com/m.ashfaqanwar" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+918197597448" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:mohd.ashfaq@live.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Akshay Kekuda</h2>
		<h4>Media and Out-Reach</h4>
		<div class="icons">
			<a href="https://www.facebook.com/akshay.kekuda" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+918867627529" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:kekuda.akshay@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Anil Kumar N</h2>
		<h4>Corporate Hospitality</h4>
		<div class="icons">
			<a href="https://www.facebook.com/anil.kumarn.56" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919481364948" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:anilkumar15.1995@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Sandeep Nadendla</h2>
		<h4>Student Hospitality</h4>
		<div class="icons">
			<a href="#" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+917204621510" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:nadendlasandeep@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Shreedhar Bhat</h2>
		<h4>Creative Head</h4>
		<div class="icons">
			<a href="https://www.facebook.com/shreedhar.bhat.927" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919449455629" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:bhatshreedhar33@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Sarthak Acharya</h2>
		<h4>Creative Head</h4>
		<div class="icons">
			<a href="https://www.facebook.com/sarthak.acharya" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919420684645" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:sarthak2195@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Anas Abdul Rahiman</h2>
		<h4>Inci Specials Coordinator</h4>
		<div class="icons">
			<a href="https://www.facebook.com/anas.abdulrahiman.1" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919037757567" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:anaspulikunnu@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Kevin DSouza</h2>
		<h4>Quality Management</h4>
		<div class="icons">
			<a href="https://www.facebook.com/kevin.dsouza.9615" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919449711647" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:kdsouza1496@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Amit Shenoy</h2>
		<h4>Quality Management</h4>
		<div class="icons">
			<a href="https://www.facebook.com/profile.php?id=1599485754" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919980944866" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:amitmshenoy@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Arshad Khan</h2>
		<h4>Student Council President</h4>
		<div class="icons">
			<a href="https://www.facebook.com/arshad.khan.7334" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919535503101" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:arshadark47@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Almas Parveen</h2>
		<h4>Student Council Vice President</h4>
		<div class="icons">
			<a href="https://www.facebook.com/AlmasParveen" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919986983825" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:almasprvn@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Abhishek Shanthkumar</h2>
		<h4>Technical Secretary</h4>
		<div class="icons">
			<a href="https://www.facebook.com/abhishek.shanthkumar" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+918105563395" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:studenttechnicalsecretary@nitk.edu.in" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Sourabh Shetty</h2>
		<h4>Cultural Secretary</h4>
		<div class="icons">
			<a href="https://www.facebook.com/sourabh.shetty.315" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919741568301" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:studentculturalsecretary@nitk.edu.in" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Adarsh Pai</h2>
		<h4>Treasurer</h4>
		<div class="icons">
			<a href="https://www.facebook.com/adarsh.pai.3" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919611213769" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:adarshpai96@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Srikanth N</h2>
		<h4>Joint Convenor</h4>
		<div class="icons">
			<a href="https://www.facebook.com/srikanth.nandhikonda" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+919071989030" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:nksr.1997@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
	<div class="contact">
		<h2>Rohan Poojary</h2>
		<h4>Website Lead</h4>
		<div class="icons">
			<a href="https://www.facebook.com/rohan.rpoojary" target="_blank" class="icon">
				<div class="icon-bg fb-bg"></div>
				<div class="icon-img fb-img"></div>
			</a>
			<a href="tel:+917760887581" target="_blank" class="icon">
				<div class="icon-bg phone-bg"></div>
				<div class="icon-img phone-img"></div>
			</a>
			<a href="mailto:rohanrp23@gmail.com" target="_blank" class="icon">
				<div class="icon-bg"></div>
				<div class="icon-img"></div>
			</a>
		</div>
	</div>
</div>
</main>
</body>
</html>