<!DOCTYPE html>
<html lang="en-US">
<head>
	<title>Incident - NITK Surathkal | South India's Second Largest Cultural Fest</title>
	<meta name="description" content="Incident is the annual cultural festival of NITK, Surathkal and is one of the 
			biggest of its kind in India. Incident 2017 will be held from March 1 - 5 with over 60 events and artists like
			DJ Chetas, Lost Stories, Baiju Dharmajan, Karthick Iyer and bands like The Local Train, Syndicate presenting
			breathtaking performance."/>
	<meta name="keywords" content="Incident, Inci, Incident Fest, Incident 2017, Inci 2017, Incident 17, NITK Fest, NITK Cultural Fest,
	 Inci 17, Incident College Festival, Incident Cultural Fest, South India's Second Largest Fest, Karnataka's Largest Fest"/>
	<link rel="canonical" href="http://www.incident.co.in"/>
	<meta name="twitter:card" content="summary_large_image"/>
	<meta name="twitter:site" content="@incident_nitk"/>
	<meta name="twitter:title" content="Incident Website">
	<meta name="twitter:description" content="Incident is the annual cultural festival of NITK, Surathkal and will be held from
											March 1 - 5"/>
	<meta name="twitter:image" content="http://www.incident.co.in/images/twitter_card_img.png"/>

	<meta property="og:title" content="Incident"/>
	<meta property="og:description" content="Incident is the annual cultural festival of NITK, Surathkal and will be held from
											March 1 - 5"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="http://www.incident.co.in/"/>
	<meta property="og:image" content="http://www.incident.co.in/images/Logo.png"/>
	<meta property="og:image:type" content="image/png" />

	<?php include_once("headers.php") ?>
	<link href='https://fonts.googleapis.com/css?family=Rochester' rel='stylesheet'>
	<link href="https://fonts.googleapis.com/css?family=Arvo" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/01-index.min.css">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.4/TweenMax.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js"></script>

    <script async src="js/throttle.jquery.min.js"></script>
    <script async src="js/scroll.jquery.min.js"></script>
	<script async src="js/01-index.min.js"></script>
</head>
<body>
<?php include_once("loader.php"); ?>
<?php include_once("menu.php"); ?>
<main>
	<div class="mobile-bg"></div>
	<div id="main">
		<div class="bg"></div>
		<div class="land">
			<div class="lighthouse"></div>
			<div class="tree"></div>
			<div class="shop"></div>
			<div class="umbrella"></div>
			<div class="tent"></div>
			<div class="ferrywheel"></div>
			<div class="icecream"></div>
			<div class="circus"></div>
		</div>
		<div class="waves">
			<div class="wave wave1 light-blue"></div>
			<div class="wave wave2 dark-blue"></div>
			<div class="wave wave3 light-blue"></div>
		</div>
		<div class="air">
			<div class="cloud" id="cloud1"></div>
			<div class="cloud" id="cloud2"></div>
			<div class="cloud" id="cloud3"></div>
			<div class="cloud" id="cloud4"></div>
			<div class="airplane"></div>
			<div class="balloon1"></div>
			<div class="balloon2"></div>
		</div>
	</div>
	<div id="header">
		<div class="bg"></div>
		<div class="header-text">
			<?php include_once("inci-text.php") ?>
		</div>
		<div class="header-date">March 1 - 5</div>
	</div>
	<div id="about">
		<div class="about-us">
			<h1>About Incident</h1>
			<p>
				Incident is the annual cultural festival of NITK Surathkal and is one of the 
				biggest of its kind in India. This four day festival, which takes place in the month of March,
				attracts a footfall of more than 40,000 every year. Students from numerous colleges throng to 
				Incident to participate in a plethora of events as well as to witness spectacular shows. 
				Incident has featured top Indian music artists like Sunidhi Chauhan, KK, Javed Ali, 
				Salim-Sulaiman, Vishal-Shekhar as well as bands such as Indian Ocean, Motherjane, 
				Swarathma and Led Zepplica. In a nutshell, Incident plays host to a party you would 
				definitely not want to miss out on.
			</p> 
			<svg class="icon" width="500" height="500" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg" >
				<g>
					<circle cx="245" cy="116" r="29" />
					<path d="M248,158 Q274,155 269,180 L258,200
							 Q215,275 277,322 C320,355 277,383 277,383
							 Q240,405 210,377 Q192,355 207,305
							 Q230,250 217,210 Q210,170 248,158">
					</path>
					<circle cx="248" cy="158" r="1"/>
					<circle cx="269" cy="180" r="1"/>
					<circle cx="258" cy="200" r="1"/>
					<circle cx="277" cy="322" r="1"/>
					<circle cx="277" cy="383" r="1"/>
					<circle cx="210" cy="377" r="1"/>
					<circle cx="207" cy="305" r="1"/>
					<circle cx="217" cy="210" r="1"/>
				</g>
			</svg>
			<div class="about-footer">
				<div class="position">
					<h4>Suresh M Hegde</h4>
					<p>Dean Students' Welfare</p>
				</div>
				<div class="position">
					<h4>Prof. K.N. Lokesh </h4>
					<p>Director In-charge</p>
				</div>
			</div>
		</div>

		<div class="timeline">
			<div class="throttler">
				<div class="number timeline-year">
					<div class="digit">
						<div class="frame0">1</div>
						<div class="frame1">2</div>
					</div>
					<div class="digit">
						<div class="frame0">9</div>
						<div class="frame1">0</div>
					</div>
					<div class="digit">
						<div class="frame0">8</div>
						<div class="frame1">9</div>
					</div>
					<div class="digit">
						<div class="frame0">0</div>
						<div class="frame1">1</div>
					</div>
				</div>
				<p class="content">
					The fest was started by the students of the college led by Umar Teekay with a meager budget of Rs. 10000
				</p>
			</div>

			<ul class="timeline-line">
				<li class="active">
				<span class="year">1980</span>
				<p class="content">
					The fest was started by the students of the college led by Umar Teekay with a meager budget of Rs. 10000
				</p>
				</li>
				<li>
				<span class="year">1990</span>
				<p class="content">
					Fest had become legend among Mangaloreans
				</p>
				</li>
				<li>
				<span class="year">1992</span>
				<p class="content">
					The first fashion show, now named Haute Couture was held
				</p>
				</li>
				<li>
				<span class="year">2005</span>
				<p class="content">
					Pronites, presently the biggest crowd-puller, was initiated which elevated the fest to a whole new level
				</p>
				</li>
				<li>
				<span class="year">2006</span>
				<p class="content">
					Beach events were first introduced as a part of Incident
				</p>
				</li>
				<li>
				<span class="year">2012</span>
				<p class="content">
					The first advent into adventure sports like zorbing, parasailing and rappeling. Incident’s social initiative i-Care was also launched 
				</p>
				</li>
				<li>
				<span class="year">2015</span>
				<p class="content">
					Pronites was held in the Main Grounds for the first time with the performance of DJ duo Mightyfools and Vishal-Shekhar
				</p>
				</li>
			</ul>
			<div class="fest-count">
				<div class="individ-fest-count">
					<div class="name">Footfall</div>
					<div class="number">40000</div>
				</div>
				<div class="individ-fest-count">
					<div class="name">Events</div>
					<div class="number">60</div>
				</div>
				<div class="individ-fest-count">
					<div class="name">Colleges</div>
					<div class="number">110</div>
				</div>
			</div>
		</div>
	</div>
	<div class="notice-model">
		<a class="close" href="#"><i class="material-icons">close</i></a>
		<div class="notice-header">
			<h1>THANK YOU</h1>
			<h2>For making Incident 2017 the best ever!</h2>
		</div>
		<!-- <div class="notice-body">
			<a rel="nofollow" class="btn" href="http://inciquiz.nitk.ac.in">JOIN Quiz</a>
		</div> -->
	</div>
</main>
</body>
</html>