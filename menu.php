<?php include_once("includes/domain.php");?>

<div class="menu-btn">
	<div class="menu-bar"></div>
	<div class="menu-bar"></div>
	<div class="menu-bar"></div>
</div>
<div class="menu">
	<div class="menu-left">
		<ul>
			<li><a data-value="Home" href="<?php echo $domain;?>">Home</a></li>
			<li><a data-value="Inci Talks" href="<?php echo $domain;?>/inci-talks.php">Inci Talks</a></li>
			<li><a data-value="Events" href="<?php echo $domain;?>/events.php">Events</a></li>
			<li><a data-value="Proshows" href="<?php echo $domain;?>/proshows.php">Proshows</a></li>
			<li><a data-value="Accommodation" href="<?php echo $domain;?>/accommodation.php">Accommodation</a></li>
			<li><a data-value="I-Care" href="<?php echo $domain;?>/i-care.php">I-Care</a></li>
			<li><a data-value="Blog" href="<?php echo $domain;?>/blog">Blog</a></li>
			<li><a data-value="Register/Login" href="<?php echo $domain;?>/portal/">Register/Login</a></li>
			<li><a data-value="Sponsors" href="<?php echo $domain;?>/sponsors.php">Sponsors</a></li>
			<li><a data-value="Contact Us" href="<?php echo $domain;?>/contact-us.php">Contact Us</a></li>
		</ul>
	</div>
	<div class="menu-right">
		<h1>Incident '17</h1>
		<div class="links">
			<a target="_blank" href="<?php echo $domain;?>/files/brochure.pdf" class="link">
				<img class="icon" src="<?php echo $domain;?>/images/icons/brochure.png">
				<p>Brochure</p>
				<img class="bg" src="<?php echo $domain;?>/images/icons/brochure_bg.png">
			</a>
			<a target="_blank" rel="nofollow" href="<?php echo $domain;?>/files/event-schedule.pdf" class="link">
				<img class="icon" src="<?php echo $domain;?>/images/icons/schedule.png">
				<p>Schedule</p>
				<img class="bg" src="<?php echo $domain;?>/images/icons/schedule_bg.png">
			</a>
			<a target="_blank" href="<?php echo $domain;?>/files/incident-fees.pdf" class="link">
				<img class="icon" src="<?php echo $domain;?>/images/icons/fee.png">
				<p>FEE Details</p>
				<img class="bg" src="<?php echo $domain;?>/images/icons/fee_bg.png">
			</a>
		</div>
		<p>Follow Us:</p>
		<a rel="nofollow" href="https://www.facebook.com/incidenttheofficialpage/" class="social-link">
			<img class="bg" src="<?php echo $domain;?>/images/icons/fb_bg.png">
			<img src="<?php echo $domain;?>/images/icons/fb.png">
		</a>
		<a rel="nofollow" href="https://www.instagram.com/incident_nitk/" class="social-link">
			<img class="bg" src="<?php echo $domain;?>/images/icons/insta_bg.png">
			<img src="<?php echo $domain;?>/images/icons/insta.png">
		</a>
		<a rel="nofollow" href="https://twitter.com/incident_nitk" class="social-link">
			<img class="bg" src="<?php echo $domain;?>/images/icons/twitter_bg.png">
			<img src="<?php echo $domain;?>/images/icons/twitter.png">
		</a>
		<a rel="nofollow" href="https://www.youtube.com/user/nitkincident" class="social-link">
			<img class="bg" src="<?php echo $domain;?>/images/icons/youtube_bg.png">
			<img src="<?php echo $domain;?>/images/icons/youtube.png">
		</a>
		<a rel="nofollow" href="<?php echo $domain; ?>/portal/need-help.php" class="social-link">
			<img class="bg" src="<?php echo $domain;?>/images/icons/faq_bg.png">
			<img src="<?php echo $domain;?>/images/icons/faq.png">
		</a>
	</div>
</div>