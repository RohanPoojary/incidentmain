$(document).ready(function(){
    $('ul.tabs').tabs('select_tab', 'tab_id');
    $('select').material_select();
    $('.tab a')[0].click();
  });

(function () {
	var app = angular.module('incidentApp', []);

	app.controller("accountsController", ["$http", "$filter", function($http, $filter) {

		var scope = this;

		scope.headers = [];

		scope.accounts = [];
		scope.searchValue = "";
		scope.activeTab = 0;

		scope.pages = {};
		scope.pages.options = [10, 50, 100, 200, 500, 1000, 10000];
		scope.pages.rate = 100;
		scope.pages.active = 1;
		scope.pages.accounts = [];
		scope.pages.search = "";
		scope.pages.indices = [1];

		scope.initSearch = function() {
			scope.pages.search = "";
		};

		scope.setSearch = function() {
			scope.pages.search = scope.searchValue;
			scope.pages.active = 1;
			scope.refresh();
		};

		scope.setTab = function(index) {
			scope.activeTab = index;
			scope.setHeaders();
			scope.pages.active = 1;
			scope.refresh();
		};

		scope.isTab = function(index) {
			return scope.activeTab == index;
		};

		scope.setHeaders = function() {
			if(scope.activeTab == 0) {
				scope.headers = [
					["Inci-Id", "account_id"],
					["Full Name", "full_name"],
					["Email", "email"],
					["College", "college"],
					["Degree", "degree"],
					["Year", "year_of_study"],
					["MobileNo", "mobile_number"],
				];
			}else if(scope.activeTab == 1){
				scope.headers = [
					["Captain Id", "cap_id"],
					["Events", "events"],
					["Teams", "teams"],
					["Participants", "participants"],
					["Accommodation", "accomodation"]
				];
			}else {
				scope.headers = [
					["Full Name", "full_name"],
					["Email", "email"],
					["College", "college"],
					["Degree", "degree"],
					["Year", "year_of_study"],
					["MobileNo", "mobile_number"],
				];
			}
		};

		scope.getURL = function() {
			if(scope.activeTab == 0)
				return "get-accounts.php";
			else if (scope.activeTab == 1)
				return "get-participants.php";
			else
				return "get-ca-list.php";
		};

		scope.isLink = function(header) {
			if(header == "account_id")
				return true;
			else if(header == "cap_id")
				return true;
			return false;
		};

		scope.pages.setPages = function() {
			var query = "q=" + encodeURIComponent(scope.pages.search);
			var url = scope.getURL() + "?len=&" + query;
			// console.log(url);
			$http.get(url).then(function(response) {
				var page_count = Math.ceil(response.data / scope.pages.rate);
				output = [];
				for(var i=1; i <= page_count; i++) {
					output.push(i);
				}
				scope.pages.indices = output;
			});
		};

		scope.pages.isActive = function(index) {
			if(index == "first") {
				return scope.pages.active == 1;
			}else if(index == "last") {
				var page_count = Math.ceil(scope.accounts.length / scope.pages.rate);
				return scope.pages.active == page_count;
			}else {
				return index == scope.pages.active;
			}
		};

		scope.pages.setActive = function(index) {
			if(index == "prev") {
				scope.pages.active -= 1;
			}else if(index == "next") {
				scope.pages.active += 1;
			}else {
				scope.pages.active = index;
			}
			scope.refresh();
		};

		scope.filterComparator = function(actual, predicate) {
			for (var j in scope.headers) {
				var header = scope.headers[j][1];

				if(header == "accomodation")
					continue;

				if(!actual || !actual[header])
					return false;

				if(compare(actual[header], predicate))
					return true;
			}
			return false;
		};

		scope.pages.getAccounts = function() {
			// scope.pages.accounts = $filter('filter')(scope.accounts, scope.pages.search, scope.filterComparator);
			// var start_index = (scope.pages.active - 1) * scope.pages.rate;
			// var end_index = start_index + scope.pages.rate;
			// return scope.pages.accounts.slice(start_index, end_index);
			return scope.accounts;
		}

		scope.refresh = function() {
			scope.loading = true;
			var query = "q=" + encodeURIComponent(scope.pages.search);
			var limit = "limit=" + scope.pages.rate;
			var offset = "offset=" + scope.pages.rate * (scope.pages.active - 1);
			var url = scope.getURL() + "?" + query + "&" + limit + "&" + offset;
			// var url = scope.getURL();
			console.log(url);
			$http.get(url).then(function(response) {
				scope.loading = false;
				scope.accounts = response.data;
				scope.pages.accounts = response.data;
				scope.pages.setPages();
			});
		};

		scope.download = function() {
			var output = "";
			var accounts = scope.pages.getAccounts();
			var headers = [];
			for(var j in scope.headers)
				headers.push(scope.headers[j][0]);
			headers.push("\n");
			output = headers.toString();
			for(var i in accounts) {
				var account = accounts[i];
				var row = [];
				if(scope.isTab(1)) {
					var t_headers = scope.headers;
					var first_val = account['cap_id'];
					var last_val = "";
					if(account['accomodation']) 
						last_val = account['accomodation'].join(' | ');
					if(!account['events']) {
						row.push([first_val, '', '', last_val]);
					}
					for(var j in account['events']) {
						row.push(first_val);
						row.push(account['events'][j]);
						row.push(account['teams'][j]);
						var participants = account['participants'][j].join(' | ');
						row.push(participants);
						row.push(last_val);
						row.push('\n');
						output += row.toString();
						first_val = "";
						row = [];
						last_val = "";
					}
				}else
					for(var j in scope.headers) {
						var header = scope.headers[j][1];
						row.push(account[header]);
					}
				row.push("\n");
				output += row.toString();
			}
			document.location = 'data:text/csv,' + encodeURIComponent(output);
		};

		scope.setTab(0);
	}]);

	///////////////////////////////////////////////////////////////
	var accountApp = angular.module('accountApp', []);
	accountApp.controller("accountController", ["$http", function($http) {
		var scope = this;
		var id = getQuery(window.location.href, "id");
		scope.account = {};
		
		$http.get("get-accounts.php?q=" + id ).then(function(response) {
			scope.account = response.data[0];
		});
	}]);
	
})();


function getQuery(url, query ) {
        query = query.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var expr = "[\\?&]" + query + "=([^&#]*)";
        var regex = new RegExp( expr );
        var results = regex.exec( url );
        if( results !== null ) {
            return results[1];
        } else {
            return false;
        }
 }

 function compare(value, predicate) {
 	if(value.constructor == Array) {
 		var output = false;
 		for(var i in value) {
 			var c = compare(value[i], predicate);
 			output = output || c;
 		}
 		return output;
 	}
 	else {
 		var output = value.toUpperCase().match(predicate.toUpperCase());
 		return output != null;
 	}
 }