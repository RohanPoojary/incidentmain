$(document).ready(function() {
	var width = $(window).width();

	// Airplane Movement
	var airplane = ".air .airplane";
	var airplane_width = $(airplane).width();
	TweenMax.to($(airplane), 50, {x: -width-airplane_width, repeat: -1, repeatDelay: 2, ease:Power0.easeNone});

	// Balloon1 Movement
	var balloon1 = ".balloon1";
	var balloon1_width = $(balloon1).width();
	var balloon1_offset = width - $(balloon1).offset().left;
	TweenMax.to($(balloon1), 35, {x: balloon1_offset, ease:Power0.easeNone, onComplete: function () {
		TweenMax.set($(balloon1), {x: -width - balloon1_width});
		TweenMax.to($(balloon1), 85, {x: balloon1_width + width, repeat: -1, ease:Power0.easeNone});
	}});

	// Balloon1 Movement
	var balloon2 = ".balloon2";
	var balloon2_width = $(balloon2).width();
	var balloon2_offset = $(balloon2).offset().left + balloon1_width;
	TweenMax.to($(balloon2), 10, {x: -balloon2_offset, ease:Power0.easeNone, onComplete: function () {
		TweenMax.set($(balloon2), {x: width});
		TweenMax.to($(balloon2), 100, {x: -balloon2_width-width, repeat: -1, ease:Power0.easeNone});
	}});

	// Cloud1 Movement
	var cloud1 = "#cloud1";
	var cloud1_width = $(cloud1).width();
	var cloud1_offset = $(cloud1).offset().left + cloud1_width;
	TweenMax.to($(cloud1), 140, {x: -cloud1_offset, ease:Power0.easeNone, onComplete: function () {
		TweenMax.set($(cloud1), {x: width});
		TweenMax.to($(cloud1), 175, {x: -cloud1_width-width, repeat: -1, ease:Power0.easeNone});
	}});

	// Cloud2 Movement
	var cloud2 = "#cloud2";
	var cloud2_width = $(cloud2).width();
	var cloud2_offset = $(cloud2).offset().left + cloud2_width;
	TweenMax.to($(cloud2), 130, {x: -cloud2_offset, ease:Power0.easeNone, onComplete: function () {
		TweenMax.set($(cloud2), {x: width});
		TweenMax.to($(cloud2), 165, {x: -cloud2_width-width, repeat: -1, ease:Power0.easeNone});
	}});

	// Cloud3 Movement
	var cloud3 = "#cloud3";
	var cloud3_width = $(cloud3).width();
	var cloud3_offset = $(cloud3).offset().left + cloud3_width;
	TweenMax.to($(cloud3), 115, {x: -cloud3_offset, ease:Power0.easeNone, onComplete: function () {
		TweenMax.set($(cloud3), {x: width});
		TweenMax.to($(cloud3), 150, {x: -cloud3_width-width, repeat: -1, ease:Power0.easeNone});
	}});

	var cloud4 = "#cloud4";
	var cloud4_width = $(cloud4).width();
	var cloud4_offset = $(cloud4).offset().left + cloud4_width;
	TweenMax.to($(cloud4), 115, {x: -cloud4_offset, ease:Power0.easeNone, onComplete: function () {
		TweenMax.set($(cloud4), {x: width});
		TweenMax.to($(cloud4), 150, {x: -cloud4_width-width, repeat: -1, ease:Power0.easeNone});
	}});

	// Waves
	var waves = ".waves";
	var displace = $(waves).children(".wave1").width()/ 2;
	waveFlow($(waves).children(".wave1"), 40, displace, -1);

	var displace = $(waves).children(".wave2").width()/ 2;
	waveFlow($(waves).children(".wave2"), 90, displace, 1);

	var displace = $(waves).children(".wave3").width()/ 2;
	waveFlow($(waves).children(".wave3"), 100, displace, -1);

	// About Us animations
	var timeline = $("#about .timeline");
	var throttler = timeline.children(".throttler");

	timeline.find(".timeline-line li").click(function() {
		$(this).addClass("active").siblings().removeClass("active");

		var year = $(this).children(".year").text();
		throttler.children(".number").attr("data-finish", year).spin(50);

		var content = $(this).children(".content").text();
		var content_timeline = new TimelineMax();
		content_timeline
			.to(throttler.children(".content"), 0.5, {alpha: 0, ease: Power0.easeNone, onComplete: function() {
				throttler.children(".content").text(content);
				TweenMax.set(throttler.children(".content"), {y: "50%"})
			}})
			.to(throttler.children(".content"), 0.5, {y: "0%", alpha: 1, ease: Power0.easeNone});
	});

	$(".notice-model .close").click(function(e) {
		e.preventDefault();
		TweenMax.to($(this).parent(), 1, {top: "+=40%", alpha: 0, ease: Power2.easeOut, onComplete: function() {
			$(".notice-model").css("display", "none");
		}});
		return false;
	});

})

function waveFlow(obj, time, displace, flag) {
	if(flag > 0) {
		var neg_wave_width = "-=" + String(displace);
		TweenMax.to(obj, time, {x: neg_wave_width, ease:Power0.easeNone, onComplete:function(){
			var pos_wave_width = "+=" + String(displace);
			TweenMax.set(obj, {width: pos_wave_width});
			waveFlow(obj, time, displace, flag);
		}});
	}else {
		var neg_wave_width = "+=" + String(displace);
		TweenMax.to(obj, time, {x: neg_wave_width, ease:Power0.easeNone, onComplete:function(){
			var pos_wave_width = "+=" + String(displace);
			TweenMax.set(obj, {width: pos_wave_width});
			waveFlow(obj, time, displace, flag);
		}});
	}
}