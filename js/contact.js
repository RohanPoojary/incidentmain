$(document).ready(function() {
	if($(window).width < 500) {
		return;
	}

	$(".contact .phone-img").click(function() {
		var number = $(this).parent().attr("href");
		number = number.slice(4);
		var contact = $(this).parentsUntil(".contacts").last();
		var number_elem = contact.children(".number");
		if(!number_elem.text()) {
			number_elem.text(number);
		}else {
			number_elem.text("");
		}
		return false;
	})
})