$(document).ready(function (){
	$('select').material_select();
	
	$(".toast-content").each(function() {
		var val = $(this).text();
		if(val) {
			Materialize.toast(val, 5000);
		}
	});

	$('select').on("change", function() {
		var team_events = [1, 3, 7, 8, 9, 10];
		var val = Number($(this).val());
		if(team_events.indexOf(val) >= 0) {
			$("#team_name_row").slideDown(1000);
		}else {
			$("#team_name_row").slideUp(1000);
		}
	});

	$("#events").each(function(){
		var event_val = getQuery(window.location.href, "event");
		if(event_val) {
			event_val = decodeURIComponent(event_val);
			event_val = event_val.replace(/\+/g, " ");
			var options = $(this).find("option");
			var exists = false;
			options.each(function() {
				if($(this).text() ==  event_val) {
					exists = true;
				}
			});
			if(exists) {
				options.each(function() {
					if($(this).text() ==  event_val) {
						$(this).attr("selected", "");
					}
					else {
						$(this).attr("selected", null);
					}
					$('select').material_select();
				});
			}
		}
	});

	var accomodation_row = $("#no-of-accomodation");
	accomodation_row.slideUp();
	$("#accomodation").click(function() {
		accomodation_row.slideToggle();
	})

	$(".participant .btn").on("click", function() {
		var parent = $(this).parent().clone();
		parent.find("input").val("");
		parent.find(".btn").remove();
		$(this).parent().after(parent);
		return false;
	});

	$(".input-field").each(function() {
		$(this).children("label").click(function() {
			var sibling = $(this).siblings("input");
			if(sibling.length) {
				$(this).toggleClass("active");
				sibling.focus();
			}
		});
	});

	$(".nestaway-widget").each(function() {
		$("#nestaway-widget").slideUp(100);
		$(this).children(".nestaway-btn").click(function(e) {
			$("#nestaway-widget").slideToggle(1500);
			return false;
		});
	})
});

function validateRegister() {
	var participants = "";
	var cap_div = $(".participants-ids input").eq(0);
	if(cap_div.length >= 0) {
		$("#account_id").val(cap_div.val());
	}

	$(".participants-ids input").each(function() {
		var value = $(this).val();
		if(value)
			participants += value + ",";
	});

	participants = participants.slice(0, -1)
	$("#participants").val(participants);
	return true;
}

function getQuery(url, query ) {
    query = query.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var expr = "[\\?&]" + query + "=([^&#]*)";
    var regex = new RegExp( expr );
    var results = regex.exec( url );
    if( results !== null ) {
        return results[1];
    } else {
        return false;
    }
 }