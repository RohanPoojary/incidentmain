var PIECES;

$.fn.shuffle = function() {
   var parent = $(this).parent();
    var divs = parent.children();
    while (divs.length) {
        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
    }
};

document.onreadystatechange = function(e) {
    var loader = $(".loader");
    if(document.readyState=="interactive") {
        var all = $("img, link");
        PIECES = $("#Logo path");
        PIECES.shuffle();
        // console.log(all.length);
         for (var i=0; i < all.length; i++) {
            set_ele(all[i]);
        }
    }
}

function check_element() {
    var all =  $("img, link");
    var totalele = all.length;
    var tot_offset = 2000;

    var per_inc = 100 / all.length;
    // console.log(per_inc);

    var loader = $(".loader");
    // if($(ele).on()) {
    var prog_width = per_inc + Number(loader.attr("data-progress"));
    
    var max_pieces = prog_width * PIECES.length / 100;
    max_pieces = Math.ceil(max_pieces);
    // console.log(pieces);

    var offset = tot_offset - Number(prog_width * tot_offset / 100);
    // console.log(offset);
    // console.log(max_pieces);
    PIECES.each(function() {
        // console.log($(this).index());
        var opacity_val = Number($(this).index() < max_pieces);
        // console.log(opacity_val, max_pieces);
        $(this).animate({opacity: opacity_val}, 100);
    })

    loader.attr("data-progress", prog_width);

    $("#Pentagon path").animate(
    { 
        strokeDashoffset: offset
    } ,100, function() {
        // console.log($("#Logo path").css("opacity"));
    });
}

function set_ele(set_element) {
    set_element.onload = check_element;
}

window.onload = function(e) {
     var loader = $(".loader-wrapper");
     var PIECES = $("#Logo path");
     PIECES.animate({opacity: 1}, 100);
    $("#Pentagon path").animate(
    { 
        strokeDashoffset: 0
    } ,100, function () {
        loader.animate({top: "-100%"}, 1500, function() {
        	$(this).css("display", "none");
            homeAnimation();   
        });
    });
}

function homeAnimation() {
    var incident = $("#header .header-text g");
    if(incident.length) {
         var inciTween = new TimelineMax();
        inciTween
            .to(incident, 4, {strokeDashoffset: 0, ease: Power0.easeNone})
            .to(incident, 1, {fill: "#102E41", stokeWidth: 0, ease: Power0.easeNone}, 3)
            .fromTo("#header .header-date", 1.5, {opacity: 0, left: "-=5%"}, {opacity: 0.8, left: "+=5%", ease: Power1.easeOut}, 3);
        // $("#home .down-button").addClass("animate");
    }
}
