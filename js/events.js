$(document).ready(function () {

	$.fn.toggleVisibility = function(time, callback) {
		return $(this).each(function() {
			var display = $(this).css("display");
			$(this).stop(true, true);

			if(display == "none") {
				$(this).css({opacity: 0, display: "block"});
				$(this).animate({opacity: 1}, time, callback);
			}else {
				$(this).animate({opacity: 0}, time, function() {
					$(this).css({display: "none"});
					callback();
				});
			}
		});
	};

	var sidebar = $(".side-bar");

	// Initialization
	var arr = [];
	arr.push($(".side-bar").children(".scroll-bar"));
	initialize(arr);

	// Scroll Animations
	$(window).scroll(function() {
		var window_top = $(this).scrollTop();
		var window_bottom = window_top + ($(this).height() / 2);

		$(".events .event").each(function() {
			var top_offset = $(this).offset().top;
			var id = $(this).attr("id");

			if(top_offset >= window_top && top_offset < window_bottom) {
				var elem = $(".side-bar li[data-target='#" + id + "']");
				elem.siblings().removeClass("active").children(".underline").removeClass("animate");
				elem.addClass("active").children(".underline").addClass("animate");
				updateScroll(true);
			}
		});
	});


	// SideBar animations
	sidebar.find("li").click(function() {
		$(this).siblings().removeClass("active").children(".underline").removeClass("animate");
		$(this).addClass("active");
		updateScroll();
	})

	// Events Click Anuimations
	var sub_events = $(".event");
	sub_events.find(".sub-event").click(function() {
		var target_element = $(this).attr("data-target");

		if(target_element.length != 0) {
			$(this).addClass("active").siblings().removeClass("active");
			$(target_element).addClass("active").siblings().removeClass("active");
		}
	});

	// Contact Button
	$(".event .contact").each(function() {
		var desc = $(this).parent().children(".desc");
		var contact = $(this).parent().children(".contact-desc");

		$(this).click(function() {
			$(this).toggleClass("active");
			if($(this).hasClass("active")) {
				desc.toggleVisibility(700, function() {
					contact.toggleVisibility(700);
				});
			}else {
				contact.toggleVisibility(700, function() {
					desc.toggleVisibility(700);
				});
			}
		});
	});
});

function is_mobile() {
	return $(window).width() < 420;
}

function updateScroll(set) {
	// Side bar animation
	var scrollbar = $(".side-bar").children(".scroll-bar");
	var element = $(".side-bar li.active");
	var scrollbar_height = scrollbar.height() / 2;
	var target_element = $(element.attr("data-target"));
	var top_height = element.offset().top + scrollbar_height  -  scrollbar.parent().offset().top;
	// var top_height = element.offset().top;
	// console.log(scrollbar.parent().offset().top);

	if(set) {
		scrollbar.css({top: top_height});
	}else {
		TweenMax.to($("html, body"), 1, {scrollTop: target_element.offset().top, ease: Power1.easeOut});
		scrollbar.animate({top: top_height}, 500);
	}
}

function initialize(arr) {
	updateScroll(true);
}

function overlay_animation(element, time, velocity) {
	var wrapper_element = '<span class="overlay-cover"></span>';
	var append_element = '<div class="overlay"></div>';

	element.removeClass("req-overlay");
	element.append(append_element);
	element.wrapInner(wrapper_element);

	var overlay = element.children();
	overlay.css("color", "transparent");

	if (velocity) {
		time = element.width()/time;
	}

	var timeline = new TimelineMax();
	timeline
		.to(overlay.children(".overlay"), time, {width: "100%", ease: Power2.easeIn, onComplete: function() {
			overlay.css("color", "");
			overlay.children(".overlay").css("right", 0);
		}})
		.to(overlay.children(".overlay"), time, {width: "0%", ease: Power2.easeIn});
}