$(document).ready(function() {

	if($(window).width() < 500) {
		$(".frame-fit").remove();
		return;
	}

	$(".song a").click(function(e) {
		if($(this).parent().hasClass("more-link"))
			return true;
		e.preventDefault();
		$(this).parent().addClass("active").siblings().removeClass("active");
		var link = $(this).attr("href");
		var parent = $(this).parentsUntil(".events").last();
		var iframe = parent.find("iframe");
		if(!iframe.length)
			return true;
		link = getQuery(link, "v");
		link = "https://www.youtube.com/embed/" + link;
		iframe.attr("src", link);
		return false;
	});
});

function getQuery(url, query ) {
        query = query.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var expr = "[\\?&]" + query + "=([^&#]*)";
        var regex = new RegExp( expr );
        var results = regex.exec( url );
        if( results !== null ) {
            return results[1];
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        } else {
            return false;
        }
 }