$(document).ready(function() {
	var menu_btn = $(".menu-btn");
	var menu = $(".menu");
	var body = $("main");
	menu_btn.click(function() {
		$(this).toggleClass("close");
		menu.toggleClass("close");
	});

	var active_link = getURL(document.location.href);
	var hash = document.location.hash;
	if(hash) {
		scrollTo(hash);
	}

	$(".menu .menu-left a").each(function() {
		var link = getURL($(this).attr("href"));
		if(active_link.match(link)) {
			$(this).parent().addClass("active").siblings().removeClass("active");
		}
	})

	menu.find(".menu-left a").click(function(e) {
		var link = getURL($(this).attr("href"));
		var hash = this.hash;
		if(hash) {
			if(link.match(active_link)) {
				console.log("Same Page");
				menu.removeClass("close");
				menu_btn.removeClass("close");
				scrollTo(hash);
				return false;
			} else {
				return true;
			}
		}
		return true;
	});
});

function scrollTo(hash) {
	var top_offset = $(hash).offset().top;
	TweenMax.to("html, body", 2, {scrollTop: top_offset, ease:Power3.easeOut});
}

function getURL(link) {
	var len = link.length;
	var domain = document.domain;
	var domain_length = domain.length;
	var start_index = link.indexOf(domain);
	var rel_link = link.slice(start_index + domain_length, len);
	if(rel_link == "/")
		return rel_link + "index.php";
	return rel_link;
}