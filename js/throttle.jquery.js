function getRotation(fromVal, toVal, ind) {
	return Math.abs(Number(toVal.slice(0, ind+1)) - Number(fromVal.slice(0, ind+1)));
}


$.fn.trottle = function(timeOut, direction) {
	return $(this).each(function() {
		var top_offset = -(direction + 1) * $(this).height() / 2;
		var top_offset_final = (direction - 1) * $(this).height() / 2;

		$(this).children().animate({
			top: top_offset
		}, timeOut, function() {
			var value = Number($(this).text());
			var offset = 5 - (4 * direction);
			$(this).text((value + offset) % 10);
			$(this).css("top", top_offset_final);
		});
	});
};


$.fn.spin = function(speed) {
	return $(this).each(function() {
		$(this).find(".digit").children().stop(true, true);

		var frames = $(this).find(".digit").children();

		if(frames.css("top") == "-" + frames.parent().css("height")) {
			frames.each(function() {
				$(this).css("top", 0);
				var value = Number($(this).text());
				$(this).text((value + 1) % 10);
			});
		}

		var cur_val = $(this).find(".frame0").text();
		var target_val = $(this).attr("data-finish");
		var diff = Number(target_val) - Number(cur_val);
		var direction = 1;

		if(diff < 0) {
			direction = -1;
			diff *= -1;
			$(this).children().each(function() {
				$(this).children().css("top", -$(this).height()).each(function() {
					var value = Number($(this).text());
					$(this).text((value + 9) % 10);
				});
			})
		}

		$(this).find(".digit").each(function() {
			var ind = $(this).index();
			var len = $(this).siblings().length + 1;
			var rotations = getRotation(cur_val, target_val, ind);
			var timePeriod =  diff * speed / rotations;
	
			for(var i=0; i < rotations; i++) {
				$(this).trottle(timePeriod, direction);
			}
		})
	})
};