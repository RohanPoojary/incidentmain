(function($) {
	$(document).ready(function() {


		$.fn.customMove = function(movement) {
			return $(this).each(function() {
				
					var obj_offset = $(this).offset().top;
					if(!$(this).hasClass("complete")) {
						var wtop = $(window).scrollTop() + $(window).height();
						var topOffset = $(this).offset().top + ($(this).height() / 2);
						if(topOffset <= wtop) {
							TweenMax.to($(this), 1.5, {top: "-=80px", opacity:"1", ease: Power2.easeOut});
							$(this).addClass("complete");
						}
					}
			});
		};

		if($(window).width() < 500)
				return;

		$(".row .sponsors").each(function () {
			var offset = 80;
			$(this).css({top: offset, opacity: 0});
		});

		$(window).scroll(function() {
			$(".row .sponsors").customMove();
		});

		$(".row .sponsors").customMove();

	});
})(jQuery)