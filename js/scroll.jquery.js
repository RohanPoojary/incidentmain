$(document).ready(function() {

	if($(window).width() <= 1000)
		return;
	
	var controller = new ScrollMagic.Controller();


	// Disc Animation
	var disk = $("#about .timeline .throttler");
	var timeline = $("#about .timeline .timeline-line");

	var timelineScroll = new TimelineMax();
	timelineScroll
		.from(disk, 10, {left: "-=100%", ease: Power0.easeNone})
		.from(timeline, 10, {top: "+=100%", ease: Power0.easeNone}, 2);
		// .to(disk, 5, {left: "-=100%", ease: Power0.easeNone})
		// .to(timeline, 2, {opacity: 0, ease: Power0.easeNone});

	var mainScene = new ScrollMagic.Scene({
		triggerElement: ".main",
		duration: "100%",
		triggerHook: 0,
	})
	.setTween(timelineScroll)
	// .addIndicators({
	// 	name: "Main Scene",
	// 	color: "black",
	// 	colorStart: "#aabb3c"
	// })
	.addTo(controller);



	// About Us content Animation
	var about_us = $(".about-us");
	var count_obj = $("#about .timeline .fest-count");

	var timelineAbout = new TimelineMax();
	timelineAbout
		.from(about_us, 2, {top: "+=100"}, 0)
		.from(about_us.children(), 5, {alpha: 0, ease: Power0.easeNone}, 1)
		.from(about_us.children("h1"), 5, {top: "-=10%", ease: Power0.easeNone}, 0)
		.from(about_us.children("p"), 5, {top: "+=30%", ease: Power0.easeNone}, 0)
		.from(count_obj.find(".name"), 3, {top: "+=500%", ease: Power0.easeNone}, 2)
		.from(count_obj.find(".number"), 3, {top: "+=300%", ease: Power0.easeNone}, 2);

	var aboutScene = new ScrollMagic.Scene({
		triggerElement: "#about",
		duration: "80%",
		triggerHook: 0.8,
	})
	.setTween(timelineAbout)
	// .addIndicators({
	// 	name: "About Scene",
	// 	color: "black",
	// 	colorStart: "#aabb3c"
	// })
	.addTo(controller);

	// var aboutPinScene = new ScrollMagic.Scene({
	// 	triggerElement: "#about",
	// 	triggerHook: 0,
	// })
	// .setPin('#about', {pushFollowers: false})
	// // .addIndicators({
	// // 	name: "About Pin Scene",
	// // 	color: "black",
	// // 	colorStart: "#aabb3c",
	// // 	colorEnd: "#3bcde4"
	// // })
	// .addTo(controller);

})