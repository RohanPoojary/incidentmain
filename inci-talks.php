<!DOCTYPE html>
<html>
<head>
	<title>Inci Talks | Incident - NITK Surathkal</title>
	<meta name="description" content="Incident 2017 presents Inci Talks which hosts famous personalities
								like Dr. JayaPrakash Narayan, Balaji Vishwanathan and many more at NITK Surathkal"/>
	<?php include_once("headers.php") ?>
	<link rel="stylesheet" type="text/css" href="css/events.min.css">
	<link rel="stylesheet" type="text/css" href="css/events.media.min.css">
	<link rel="stylesheet" type="text/css" href="css/02-inci-talks.min.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
	<script src="js/events.min.js"></script>
</head>
<body>
<?php include_once("loader.php"); ?>
<?php include_once("menu.php"); ?>
<main>
<div class="side-bar">
	<h1>Inci Talks</h1>
	<ul>
		<li class="active" data-target="#jayaprakash-narayan">
			<span class="underline animate">Dr.JayaPrakash Narayan</span>
		</li>
		<li data-target="#balaji-vishwanathan">
			<span class="underline">Balaji Viswanathan</span>
		</li>
		<li data-target="#prachi-tehlan">
			<span class="underline">Prachi Tehlan</span>
		</li>
		<li data-target="#the-last-kannadiga">
			<span class="underline">The Last Kannadiga</span>
		</li>
	</ul>
	<div class="scroll-bar"></div>
</div>
<div class="events">
	<div class="event" id="jayaprakash-narayan">
		<img src="images/events/jayaprakash-narayan.jpg" class="event-bg">
		<h1>Dr. JayaPrakash Narayan</h1>
		<p class="event-description">
			Dr. Jayaprakash Narayan is one of the few political leaders in India who has served the country for
			the past 16 years without much fuss and PR. He is a physician by training who went into the Indian
			Administrative Service in the aftermath of the Emergency and failure of the Janata Experiment. He
			is the founder and president of the LokSatta party, which was formed to educate the citizens of
			India about voting, rights and government.
		</p>
		<div class="more-details">
		 	<div class="left-half"></div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active">
		 			<p class="contact">Contact</p>
		 			<div>
		 				<h2 class="prizes left-align">Date: 02/03/2017</h2>
		 			</div>
		 			<p class="desc">
		 				Only 300 seats available. Register here:
		 			</p>
		 			<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Akshay Kekuda<br/>
			 				<a href="tel:8867627529">+91 88676 27529</a>
			 			</p>
			 		</div>
			 		<br/>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Inci+Talks-+Dr+JayaPrakash+Narayan">Register</a>
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="event" id="balaji-vishwanathan">
		<img src="images/events/balaji-vishwanathan.jpg" class="event-bg">
		<h1>Balaji Viswanathan</h1>
		<p class="event-description">
			Mr Viswanathan remains the most followed person in the world on the question and answer forum -
			Quora with over 2,60,000 followers . He has written more than 3,500 answers with some excellent 
			insights into Indian history,culture, world politics and quite simply everything under the sky.
			<br/>
			He is the VP of products at Invento and product manager at Black Duck Software. 
		</p>
		<div class="more-details">
		 	<div class="left-half"></div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active">
		 			<p class="contact">Contact</p>
		 			<div>
		 				<h2 class="prizes left-align">Date: 03/03/2017</h2>
		 			</div>
		 			<p class="desc">
		 				Only 300 seats available. Register here:
		 			</p>
		 			<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Akshay Kekuda<br/>
			 				<a href="tel:8867627529">+91 88676 27529</a>
			 			</p>
			 		</div>
			 		<br/>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Inci+Talks-+Balaji+Viswanathan">Register</a>
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="event" id="prachi-tehlan">
		<img src="images/events/prachi-tehlan.jpg" class="event-bg">
		<h1>Prachi Tehlan</h1>
		<p class="event-description">
			Prachi Tehlan is an Indian netball and basketball player, and an actress.
			<br/><br/>
			Prachi is the former captain of the Indian Netball Team which represented India in the 2010 
			Commonwealth Games and in other major Asian Championships in 2010-11. Under her captaincy, the 
			Indian team won its first medal in 2011 South Asian Beach Games. She is one of the best examples of 
			being a student athlete turned into a corporate professional, a combination which we rarely get to 
			see. She is dynamic, confident, multi-talented and is equally interested in the field of art, 
			photography, travelling and reading.
		</p>
		<div class="more-details">
		 	<div class="left-half"></div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active">
		 			<p class="contact">Contact</p>
		 			<div>
		 				<h2 class="prizes left-align">Date: 04/03/2017</h2>
		 			</div>
		 			<p class="desc">
		 				Only 300 seats available. Register here:
		 			</p>
		 			<div class="contact-desc">
			 			<h3>Contact</h3>
			 			<p>
			 				Akshay Kekuda<br/>
			 				<a href="tel:8867627529">+91 88676 27529</a>
			 			</p>
			 		</div>
			 		<br/>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Inci+Talks-+Prachi+Tehlan">Register</a>
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="event" id="the-last-kannadiga">
		<img src="images/events/the-last-kannadiga.jpg" class="event-bg">
		<h1>The Last Kannadiga</h1>
		<p class="event-description">
			The Last Kannadiga is a social experiment. An enquiry on the idea that a language cannot survive the death of its speakers. 
			<br/>
			We will be having the screening of this amazing short movie followed by an interactive session with 
			the crew of the movie 
		</p>
		<div class="more-details">
		 	<div class="left-half"></div>
		 	<div class="right-half">
		 		<div class="sub-event-desc active">
		 			<div>
		 				<h2 class="prizes left-align">Date: 05/03/2017</h2>
		 			</div>
		 			<div class="contact-desc" style="display: block">
			 			<h3>Contact</h3>
			 			<p>
			 				Akshay Kekuda<br/>
			 				<a href="tel:8867627529">+91 88676 27529</a>
			 			</p>
			 		</div>
			 		<br/>
			 		<a class="register"  target="_blank" href="<?php echo $domain; ?>/portal/register.php?event=Inci+Talks-+The+Last+Kannadiga">Register</a>
		 		</div>
		 	</div>
		 </div>
	</div>
</div>
</main>
</body>
</html>